# exception/error handling
# you get errors which are not expected and you need to handle them.
# exception handling is available in all programming languages
# try..else..except..finally

```python
# case I
```


```python
num1 = int(input("please enter a num1:"))
num2 = int(input("please enter a num2:"))
result = num1/num2
print ("The output of the number is :{}".format(result))
```

    please enter a num1:10
    please enter a num2:2
    The output of the number is :5.0
    


```python
# case II
```


```python
num1 = int(input("please enter a num1:"))
num2 = int(input("please enter a num2:"))
result = num1/num2
print ("The output of the number is :{}".format(result))
```

    please enter a num1:ten
    


    ---------------------------------------------------------------------------

    ValueError                                Traceback (most recent call last)

    <ipython-input-3-4e94e46eb5e2> in <module>
    ----> 1 num1 = int(input("please enter a num1:"))
          2 num2 = int(input("please enter a num2:"))
          3 result = num1/num2
          4 print ("The output of the number is :{}".format(result))
    

    ValueError: invalid literal for int() with base 10: 'ten'



```python
# case III
```


```python
num1 = int(input("please enter a num1:"))
num2 = int(input("please enter a num2:"))
result = num1/num2
print ("The output of the number is :{}".format(result))
```

    please enter a num1:10
    please enter a num2:0
    


    ---------------------------------------------------------------------------

    ZeroDivisionError                         Traceback (most recent call last)

    <ipython-input-4-4e94e46eb5e2> in <module>
          1 num1 = int(input("please enter a num1:"))
          2 num2 = int(input("please enter a num2:"))
    ----> 3 result = num1/num2
          4 print ("The output of the number is :{}".format(result))
    

    ZeroDivisionError: division by zero



```python
# what are the various exception we have.
```
# 2.x
In [1]: import exceptions

In [2]: import exceptions as e

In [3]: e.
     e.ArithmeticError           e.ImportError               e.PendingDeprecationWarning e.UnicodeDecodeError
     e.AssertionError            e.ImportWarning             e.ReferenceError            e.UnicodeEncodeError
     e.AttributeError            e.IndentationError          e.RuntimeError              e.UnicodeError
     e.BaseException             e.IndexError                e.RuntimeWarning            e.UnicodeTranslateError
     e.BufferError               e.IOError                   e.StandardError             e.UnicodeWarning
     e.BytesWarning              e.KeyboardInterrupt         e.StopIteration             e.UserWarning
     e.DeprecationWarning        e.KeyError                  e.SyntaxError               e.ValueError
     e.EnvironmentError          e.LookupError               e.SyntaxWarning             e.Warning
     e.EOFError                  e.MemoryError               e.SystemError               e.WindowsError
     e.Exception                 e.NameError                 e.SystemExit                e.ZeroDivisionError
     e.FloatingPointError        e.NotImplementedError       e.TabError
     e.FutureWarning             e.OSError                   e.TypeError
     e.GeneratorExit             e.OverflowError             e.UnboundLocalError# 3.x - we dont have the exceptions module anymore in 3.x

(base) C:\Users\tuxfux\Documents\lfl-python101\modules\os>ipython
Python 3.7.3 (default, Apr 24 2019, 15:29:51) [MSC v.1915 64 bit (AMD64)]
Type 'copyright', 'credits' or 'license' for more information
IPython 7.6.1 -- An enhanced Interactive Python. Type '?' for help.

In [1]: import builtins

In [2]: builtins.
              abs()                     classmethod               Exception                 ImportError
              all()                     compile()                 exec()                    ImportWarning
              any()                     complex                   exit                      IndentationError
              ArithmeticError           ConnectionAbortedError    False                     IndexError
              ascii()                   ConnectionError           FileExistsError           input()
              AssertionError            ConnectionRefusedError    FileNotFoundError         int
              AttributeError            ConnectionResetError      filter                    InterruptedError
              BaseException             copyright                 float                     IOError
              bin()                     credits                   FloatingPointError        IsADirectoryError
              BlockingIOError           delattr()                 format()                  isinstance()
              bool                      DeprecationWarning        frozenset                 issubclass()              >
              breakpoint()              dict                      FutureWarning             iter()
              BrokenPipeError           dir()                     GeneratorExit             KeyboardInterrupt
              BufferError               display()                 getattr()                 KeyError
              bytearray                 divmod()                  globals()                 len()
              bytes                     Ellipsis                  hasattr()                 license
              BytesWarning              enumerate                 hash()                    list
              callable()                EnvironmentError          help                      locals()
              ChildProcessError         EOFError                  hex()                     LookupError
              chr()                     eval()                    id()                      map# How to deal with exceptions
# try..else..expect..finally
# try -> block of code which you feel can raise exceptions.
# else -> if your try block is not giving exceptions ,it moves to else block.
# except -> What to do in case of an exception.# except -> basically a broad term. 
# its covers all exceptions

```python
# case I - i enter one of the numbers as non-zero.

#import sys
try:
    num1 = int(input("please enter a num1:"))
    num2 = int(input("please enter a num2:"))
    result = num1/num2
except:
    print ("Please enter integer.Please make sure your denominator is non-zero")
    #sys.exit(1)
else:
    print ("The output of the number is :{}".format(result))
```

    please enter a num1:10
    please enter a num2:0
    Please enter integer.Please make sure your denominator is non-zero
    


```python
# case II - i enter alphabets

#import sys
try:
    num1 = int(input("please enter a num1:"))
    num2 = int(input("please enter a num2:"))
    result = num1/num2
except:
    print ("Please enter integer.Please make sure your denominator is non-zero")
    #sys.exit(1)
else:
    print ("The output of the number is :{}".format(result))
```

    please enter a num1:a
    Please enter integer.Please make sure your denominator is non-zero
    
## handle generic exceptions

```python
try:
    num1 = int(input("please enter a num1:"))
    num2 = int(input("please enter a num2:"))
    result = num1/num2
except (ValueError,ZeroDivisionError):
    print ("Please enter integer.Please make sure your denominator is non-zero")
    #sys.exit(1)
else:
    print ("The output of the number is :{}".format(result))
```

    please enter a num1:ten
    Please enter integer.Please make sure your denominator is non-zero
    
## further granular exception

```python
try:
    num1 = int(input("please enter a num1:"))
    num2 = int(input("please enter a num2:"))
    result = num1/num2
except ValueError:
    print ("please enter integers.")
except ZeroDivisionError:
    print ("Please make sure your denominator is non-zero")
else:
    print ("The output of the number is :{}".format(result))
```

    please enter a num1:ten
    please enter integers.
    
example:
# flipkart - big billion day.
# snapdeal - 
development - hyd - qwerty keyboard - latin
testing     - chennai - qwerty keyboard - latin
china -> no english -> hu/madrak - crashed. (utf encoding)

## finally
# try..else..expect..finally
# try -> block of code which you feel can raise exceptions.
# else -> if your try block is not giving exceptions ,it moves to else block.
# except -> What to do in case of an exception.
# finally ->
# case I : valid inputs -- try..else..finally.
# case II: invalid input -- handles exception - try..except..finally
# case III: invalid input -- not handled by exception - try..finally.. bombed out the program

```python
# tesitng
try:
    num1 = int(input("please enter a num1:"))
    num2 = int(input("please enter a num2:"))
    result = num1/num2
except ValueError:
    print ("please enter integers.")
else:
    print ("The output of the number is :{}".format(result))
finally:
    print ('All is well!!!')
    # resourses needs to be closed in the finally block.
    # db.close()
    # file.close()
    # socket.close()
```

    please enter a num1:10
    please enter a num2:0
    All is well!!!
    


    ---------------------------------------------------------------------------

    ZeroDivisionError                         Traceback (most recent call last)

    <ipython-input-23-8dae23bfe9ef> in <module>
          3     num1 = int(input("please enter a num1:"))
          4     num2 = int(input("please enter a num2:"))
    ----> 5     result = num1/num2
          6 except ValueError:
          7     print ("please enter integers.")
    

    ZeroDivisionError: division by zero



```python

```
