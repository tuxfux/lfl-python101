Installation
-------------

which version to use 2.x or 3.x ? 2.x
can both 2.x and 3.x co-exist ? yes

Linux:
(Ubuntu/linuxmint/zorion/kali)

# sudo apt-get install python (2.x)
# sudo apt-get install python3 (3.x)
# sudo apt-get install ipython (2.x)
# sudo apt-get install ipython3 (3.x)
# sudo apt-get install bpython (2.x)
# sudo apt-get install bpython3 (3.x)

Reference:
https://ipython.org/
https://bpython-interpreter.org/

windows:
---------
https://www.anaconda.com/distribution/#windows

parent site:
-------------
https://www.python.org/

IDE
----
https://stackoverflow.com/questions/81584/what-ide-to-use-for-python