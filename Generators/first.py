#!/usr/bin/python
# -*- coding: utf-8 -*-

def square(a):
    result = []
    for value in range(1,a + 1):
        result.append(value * value)
    return result

print (type(square(10)))
print (square(10))

for value in square(10):
    print (value)
    
'''
## generator
In [1]: def my_func():
   ...:     yield 1
   ...:     yield 2
   ...:     yield 3
   ...:

In [2]: my_value = my_func()

In [3]: type(my_value)
Out[3]: generator

In [4]: next(my_value)
Out[4]: 1

In [5]: next(my_value)
Out[5]: 2

In [6]: next(my_value)
Out[6]: 3

In [7]: next(my_value)
---------------------------------------------------------------------------
StopIteration                             Traceback (most recent call last)
<ipython-input-7-2f9edf5446d9> in <module>()
----> 1 next(my_value)

StopIteration:
    
## iterator

    In [9]: my_string =  "python"

In [10]: iter_string = iter(my_string)

In [11]: print type(iter_string)
<type 'iterator'>

In [12]: print iter_string
<iterator object at 0x7f559dfc7f90>

In [13]: next(iter_string)
Out[13]: 'p'

In [14]: next(iter_string)
Out[14]: 'y'

In [15]: next(iter_string)
Out[15]: 't'

In [16]: next(iter_string)
Out[16]: 'h'

In [17]: next(iter_string)
Out[17]: 'o'

In [18]: next(iter_string)
Out[18]: 'n'

In [19]: next(iter_string)
---------------------------------------------------------------------------
StopIteration                             Traceback (most recent call last)
<ipython-input-19-33186eec0b10> in <module>()
----> 1 next(iter_string)

StopIteration:

In [20]:                                                                                                                                           
    
'''