#!/usr/bin/python

def square(a):
    for value in range(1,a + 1):
        yield value * value

print (type(square(10)))
print (square(10))

for value in square(10):
    print (value)