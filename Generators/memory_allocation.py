#!/usr/bin/python
# -*- coding: utf-8 -*-
## pip install memory_profiler (python2)
## pip3 install memory-profiler (python3)


import memory_profiler as mem_profile
import random
import time

names = ['santosh','bendu','phani','Mounika','vikram']
majors = ['devops','python','django','kubernates','dockers']

print ('Memory (Before): {}Mb'.format(mem_profile.memory_usage()))

## lists of dictionaries
def people_list(num_people):
    result = []
    for i in range(num_people):
        person = {
                    'id': i,
                    'name': random.choice(names),
                    'major': random.choice(majors)
                }
        result.append(person)
    return result

def people_generator(num_people):
    for i in range(num_people):
        person = {
                    'id': i,
                    'name': random.choice(names),
                    'major': random.choice(majors)
                }
        yield person

## iterator case
#t1 = time.process_time()
#people = people_list(1000000)
#t2 = time.process_time()

t1 = time.process_time()
people = people_generator(1000000)
t2 = time.process_time()

print ('Memory (After) : {}Mb'.format(mem_profile.memory_usage()))
print ('Took {} Seconds'.format(t2-t1))

