#!/usr/bin/python

print (type([ value*value for value in range(1,11)])) # list comprehension
print (type(( value*value for value in range(1,11)))) # tuple comprehnesion

print ([ value*value for value in range(1,11)]) # list comprehension
print  (( value*value for value in range(1,11)))# tuple comprehnesion

## generate values
for value in ( value*value for value in range(1,11)):
    print (value)