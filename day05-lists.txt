Python:
data structures
* Lists
* tuples
* dictionaries

Lists are a collection of data(hetrogenous) and in a linear format.
Lists can contain duplicate values.
lists != arrays

arrays(multi dimension representation data)

refence:
https://scipy.org/

## how to create a list

n [7]: my_students = ["vikram","bendu","sree","phani","mounika","kumar"]

print (my_students)
['vikram', 'bendu', 'sree', 'phani', 'mounika', 'kumar']

type(my_students)
Out[9]: list

my_empty = []

type(my_empty)
Out[11]: list

list?
Init signature: list(iterable=(), /)
Docstring:     
Built-in mutable sequence.

If no argument is given, the constructor creates a new empty list.
The argument must be an iterable if specified.
Type:           type
Subclasses:     _HashedSeq, StackSummary, SList, _ImmutableLineList, FormattedText, NodeList, _ExplodedList, Stack, _Accumulator, LazyList, ...


my_empty = list()

type(my_empty)
Out[13]: list

# in operator

my_students
Out[22]: ['vikram', 'bendu', 'sree', 'phani', 'mounika', 'kumar']

'vikram' in my_students
Out[23]: True

'kadar' in my_students
Out[24]: False


# mutable and immutable

my_string="python"

my_string[0]
Out[31]: 'p'

my_string[0] = "T"
Traceback (most recent call last):

  File "<ipython-input-32-7317052dffb1>", line 1, in <module>
    my_string[0] = "T"

TypeError: 'str' object does not support item assignment




# list are mutable

my_students
Out[34]: ['vikram', 'bendu', 'sree', 'phani', 'mounika', 'kumar']

my_students[1]
Out[35]: 'bendu'

my_students[1] ="Bendu"


print my_students
  File "<ipython-input-37-9341154095b2>", line 1
    print my_students
                    ^
SyntaxError: Missing parentheses in call to 'print'. Did you mean print(my_students)?




my_students
Out[38]: ['vikram', 'Bendu', 'sree', 'phani', 'mounika', 'kumar']


# lists also support indexing and slicing

my_students = ["vikram","bendu","sree","phani","mounika","kumar"]

# ["vikram","bendu","sree","phani","mounika","kumar"]

#  0          1        2      3        4        5

#  -6         -5       -4     -3      -2        -1

my_students[3]
Out[45]: 'phani'

my_students[2:5]
Out[46]: ['sree', 'phani', 'mounika']


my_students[-4:-1]
Out[48]: ['sree', 'phani', 'mounika']

## function on lists

my_students
Out[50]: ['vikram', 'bendu', 'sree', 'phani', 'mounika', 'kumar']

#my_students.index

my_students.index?
Signature: my_students.index(value, start=0, stop=9223372036854775807, /)
Docstring:
Return first index of value.

Raises ValueError if the value is not present.
Type:      builtin_function_or_method

my_students.index('phani')
Out[53]: 3

my_students.index('kumar')
Out[54]: 5

#my_students.count

my_students.count?
Signature: my_students.count(value, /)
Docstring: Return number of occurrences of value.
Type:      builtin_function_or_method

my_students.count('mounika')
Out[57]: 1

#my_students.append

my_students
Out[60]: ['vikram', 'bendu', 'sree', 'phani', 'mounika', 'kumar']

my_students.append?
Signature: my_students.append(object, /)
Docstring: Append object to the end of the list.
Type:      builtin_function_or_method

my_students.append('kishan')

my_students
Out[63]: ['vikram', 'bendu', 'sree', 'phani', 'mounika', 'kumar', 'kishan']

# my_students.insert

my_students.insert?
Signature: my_students.insert(index, object, /)
Docstring: Insert object before index.
Type:      builtin_function_or_method

my_students
Out[71]: ['vikram', 'bendu', 'sree', 'phani', 'mounika', 'kumar', 'kishan']

my_students.insert(1,"kadar")

my_students
Out[73]: ['vikram', 'kadar', 'bendu', 'sree', 'phani', 'mounika', 'kumar', 'kishan']

# my_students.extend

my_students.extend?
Signature: my_students.extend(iterable, /)
Docstring: Extend list by appending elements from the iterable.
Type:      builtin_function_or_method

my_students
Out[77]: ['vikram', 'kadar', 'bendu', 'sree', 'phani', 'mounika', 'kumar', 'kishan']

my_students.extend(['ganesh','nag','deepak'])


my_students
Out[80]: 
['vikram',
 'kadar',
 'bendu',
 'sree',
 'phani',
 'mounika',
 'kumar',
 'kishan',
 'ganesh',
 'nag',
 'deepak']
 
 my_students = ["vikram","bendu","sree","phani","mounika","kumar"]

#my_students.pop

my_students.pop?
Signature: my_students.pop(index=-1, /)
Docstring:
Remove and return item at index (default last).

Raises IndexError if list is empty or index is out of range.
Type:      builtin_function_or_method

my_students.pop
Out[4]: <function list.pop(index=-1, /)>

my_students.pop()
Out[5]: 'kumar'

my_students
Out[6]: ['vikram', 'bendu', 'sree', 'phani', 'mounika']

my_students.pop(0)
Out[7]: 'vikram'

my_students
Out[8]: ['bendu', 'sree', 'phani', 'mounika']

#my_students.remove

my_students.remove?
Signature: my_students.remove(value, /)
Docstring:
Remove first occurrence of value.

Raises ValueError if the value is not present.
Type:      builtin_function_or_method

my_students
Out[11]: ['bendu', 'sree', 'phani', 'mounika']

my_students.append("sree")

my_students
Out[13]: ['bendu', 'sree', 'phani', 'mounika', 'sree']

my_students.remove("sree")

my_students
Out[15]: ['bendu', 'phani', 'mounika', 'sree']

my_fruits = ["papaya","guava","apple","cherry","dates"]

#my_fruits.reverse

my_fruits.reverse?
Signature: my_fruits.reverse()
Docstring: Reverse *IN PLACE*.
Type:      builtin_function_or_method

my_fruits.reverse()

my_fruits
Out[21]: ['dates', 'cherry', 'apple', 'guava', 'papaya']

#my_fruits.sort

my_fruits.sort?
Signature: my_fruits.sort(*, key=None, reverse=False)
Docstring: Stable sort *IN PLACE*.
Type:      builtin_function_or_method

my_fruits.sort()

my_fruits
Out[26]: ['apple', 'cherry', 'dates', 'guava', 'papaya']

my_fruits.clear?
Signature: my_fruits.clear()
Docstring: Remove all items from list.
Type:      builtin_function_or_method

my_fruits.clear()

my_fruits

Out[31]: []

## converting a string to list and vice-versa

my_string="python"

# convert the string to a list

Lmy_string = list(my_string)

Lmy_string
Out[37]: ['p', 'y', 't', 'h', 'o', 'n']

# modification to the list

Lmy_string[0]="T"

Lmy_string
Out[40]: ['T', 'y', 't', 'h', 'o', 'n']

# convert the list back to a string

delimiter=""

delimiter.join?
Signature: delimiter.join(iterable, /)
Docstring:
Concatenate any number of strings.

The string whose method is called is inserted in between each given string.
The result is returned as a new string.

Example: '.'.join(['ab', 'pq', 'rs']) -> 'ab.pq.rs'
Type:      builtin_function_or_method

delimiter.join(Lmy_string)
Out[44]: 'Tython'

delimiter=":"

delimiter.join(Lmy_string)
Out[46]: 'T:y:t:h:o:n'

delimiter=""


delimiter.join(Lmy_string)
Out[49]: 'Tython'

new_string=delimiter.join(Lmy_string)

new_string
Out[51]: 'Tython'

my_string
Out[52]: 'python'


my_sentence = "today is sunday"

Lmy_sentence = list(my_sentence)

Lmy_sentence
Out[56]: ['t', 'o', 'd', 'a', 'y', ' ', 'i', 's', ' ', 's', 'u', 'n', 'd', 'a', 'y']

# convert a sentence to a list or break into words

my_sentence.split?
Signature: my_sentence.split(sep=None, maxsplit=-1)
Docstring:
Return a list of the words in the string, using sep as the delimiter string.

sep
  The delimiter according which to split the string.
  None (the default value) means split according to any whitespace,
  and discard empty strings from the result.
maxsplit
  Maximum number of splits to do.
  -1 (the default value) means no limit.
Type:      builtin_function_or_method

my_sentence.split()
Out[59]: ['today', 'is', 'sunday']

Lmy_sentence = my_sentence.split()

Lmy_sentence
Out[61]: ['today', 'is', 'sunday']

# modifiy the list - sunday to saturday

Lmy_sentence[-1]="saturday"

Lmy_sentence
Out[64]: ['today', 'is', 'saturday']

# converting the list to sentence/string again

delimiter=" "

delimiter.join(Lmy_sentence)
Out[67]: 'today is saturday'

## aliasing

a = 1

type(a)
Out[80]: int

id(a)
Out[81]: 140711426298688

b = 1

type(b)
Out[83]: int

id(b)
Out[84]: 140711426298688

id(1)
Out[85]: 140711426298688

b = 2

id(2)
Out[87]: 140711426298720

id(b)
Out[88]: 140711426298720

## Assignment
## trivia

c=500

d=500

id(c)
Out[92]: 2373966224976

id(d)
Out[93]: 2373966222192

# why ?


# is and in

# in works on objects

# is works on memory

a
Out[100]: 1

b
Out[101]: 2

c
Out[102]: 500

d
Out[103]: 500

id(a),id(b),id(c),id(d)
Out[104]: (140711426298688, 140711426298720, 2373966224976, 2373966222192)

c = 1

id(c)
Out[106]: 140711426298688

a is c
Out[107]: True

c is d
Out[108]: False

c in d
Traceback (most recent call last):

  File "<ipython-input-109-134cf30a5e47>", line 1, in <module>
    c in d

TypeError: argument of type 'int' is not iterable




c in [d,d]
Out[110]: False

c == d
Out[111]: False

c
Out[112]: 1

d
Out[113]: 500

e=500

d
Out[115]: 500

e
Out[116]: 500

d == e
Out[117]: True

d is e
Out[118]: False


## soft copy

The concept of sharing the same memory location for multiple
objects is called soft copy.

my_num = [1,2,3]

id(my_num),id(my_num[0]),id(my_num[1]),id(my_num[2])
Out[127]: (2373966279944, 140711426298688, 140711426298720, 140711426298752)

my_dupli = my_num

my_dupli
Out[129]: [1, 2, 3]

id(my_dupli),id(my_dupli[0]),id(my_dupli[1]),id(my_dupli[2])
Out[130]: (2373966279944, 140711426298688, 140711426298720, 140711426298752)


n [131]: my_dupli[0]
Out[131]: 1

my_dupli[0] = "one"

my_dupli
Out[133]: ['one', 2, 3]

my_num
Out[134]: ['one', 2, 3]

my_dupli = ["11","12","13"]

## deep copy
deep copy is a concept where we need a new memory block 
for every object

my_dupli
Out[136]: ['11', '12', '13']

my_num
Out[137]: ['one', 2, 3]

id(my_dupli),id(my_dupli[0]),id(my_dupli[1]),id(my_dupli[2])
Out[138]: (2373966530120, 2373890424760, 2373890465840, 2373890465896)

id(my_num),id(my_num[0]),id(my_num[1]),id(my_num[2])
Out[139]: (2373966279944, 2373900774568, 140711426298720, 140711426298752)


### soft and deep


a = ["a","b","c"]

id(a)
Out[142]: 2373946982792

id(a),id(a[0]),id(a[1]),id(a[2])
Out[143]: (2373946982792, 2373860756480, 2373860754800, 2373860617552)

# soft copy - memory is shared

b = a

id(b),id(b[0]),id(b[1]),id(b[2])
Out[146]: (2373946982792, 2373860756480, 2373860754800, 2373860617552)

# copy

import copy

copy.deepcopy?
Signature: copy.deepcopy(x, memo=None, _nil=[])
Docstring:
Deep copy operation on arbitrary Python objects.

See the module's __doc__ string for more info.
File:      c:\users\tuxfux\anaconda3\lib\copy.py
Type:      function

c = copy.deepcopy(a)

id(a),id(a[0]),id(a[1]),id(a[2])
Out[151]: (2373946982792, 2373860756480, 2373860754800, 2373860617552)

id(b),id(b[0]),id(b[1]),id(b[2])
Out[152]: (2373946982792, 2373860756480, 2373860754800, 2373860617552)

id(c),id(c[0]),id(c[1]),id(c[2])
Out[153]: (2373966465352, 2373860756480, 2373860754800, 2373860617552)

e = a[:]

e is a
Out[155]: False

a is b
Out[156]: True

a
Out[157]: ['a', 'b', 'c']

c
Out[158]: ['a', 'b', 'c']

a is c
Out[159]: False

c[0] = "apple"

c
Out[161]: ['apple', 'b', 'c']

a
Out[162]: ['a', 'b', 'c']

id(a),id(a[0]),id(a[1]),id(a[2])
Out[163]: (2373946982792, 2373860756480, 2373860754800, 2373860617552)

id(c),id(c[0]),id(c[1]),id(c[2])
Out[164]: (2373966465352, 2373966190440, 2373860754800, 2373860617552)

