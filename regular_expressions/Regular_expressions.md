# what is regular expressions/pattern matching/regex/re ?

match your patterns
reference - https://docs.python.org/2/library/re.html

```python
# call your gf/bf for a movie -> 2**3 = 8

answer = input("will you come to the movie - yes/no ?")
if answer == 'yes':
    print ("you are welcome!!")
else:
    print ("Better luck next time")
```

    will you come to the movie - yes/no ?Yes
    Better luck next time
    


```python
# re module
```


```python
import re
```


```python
dir(re)
```




    ['A',
     'ASCII',
     'DEBUG',
     'DOTALL',
     'I',
     'IGNORECASE',
     'L',
     'LOCALE',
     'M',
     'MULTILINE',
     'Match',
     'Pattern',
     'RegexFlag',
     'S',
     'Scanner',
     'T',
     'TEMPLATE',
     'U',
     'UNICODE',
     'VERBOSE',
     'X',
     '_MAXCACHE',
     '__all__',
     '__builtins__',
     '__cached__',
     '__doc__',
     '__file__',
     '__loader__',
     '__name__',
     '__package__',
     '__spec__',
     '__version__',
     '_cache',
     '_compile',
     '_compile_repl',
     '_expand',
     '_locale',
     '_pickle',
     '_special_chars_map',
     '_subx',
     'compile',
     'copyreg',
     'enum',
     'error',
     'escape',
     'findall',
     'finditer',
     'fullmatch',
     'functools',
     'match',
     'purge',
     'search',
     'split',
     'sre_compile',
     'sre_parse',
     'sub',
     'subn',
     'template']




```python
## re.match
# pattern is a subset of string.

help(re.match)
```

    Help on function match in module re:
    
    match(pattern, string, flags=0)
        Try to apply the pattern at the start of the string, returning
        a Match object, or None if no match was found.
    
    


```python
## p is a subset of python
## py is a subset of python
## th is a subset of python
## pythons is not a subset of python
## noh is not a subset of python

## if there is a match, the conditional block will see it as True else False.

my_string = "python"
```


```python
re.match("p",my_string)
```




    <re.Match object; span=(0, 1), match='p'>




```python
re.match("py",my_string)
```




    <re.Match object; span=(0, 2), match='py'>




```python
re.match("th",my_string) # no match
```


```python
# the pattern has Pyt which is a upper case.
re.match("Pyt",my_string) # no match
```


```python
#re.match("Pyt",my_string,re.IGNORECASE)
#or
re.match("Pyt",my_string,re.I)
```




    <re.Match object; span=(0, 3), match='pyt'>




```python
# call your gf/bf for a movie -> 2**3 = 8

import re

answer = input("will you come to the movie - yes/no ?")
#if answer == 'yes':
if re.match(answer,'yes',re.I):
    print ("you are welcome!!")
else:
    print ("Better luck next time")
```

    will you come to the movie - yes/no ?Ye
    you are welcome!!
    


```python
## re.search
```


```python
help(re.search)
```

    Help on function search in module re:
    
    search(pattern, string, flags=0)
        Scan through string looking for a match to the pattern, returning
        a Match object, or None if no match was found.
    
    


```python
my_sentence = "I love python."
```


```python
re.search("python",my_sentence) # match
```




    <re.Match object; span=(7, 13), match='python'>




```python
re.match("python",my_sentence) # no match
```


```python
## special characters
```


```python
# . -> dot -> represents one character.
```


```python
my_string="python"
```


```python
re.match('...',my_string)
```




    <re.Match object; span=(0, 3), match='pyt'>




```python
# group
# only if your pattern matches a string , then only we can use group.
# if not it will give an exception.
```


```python
re.match('...',my_string).group()
```




    'pyt'




```python
# ^ - caret - The beginning of a sentence/string or line.
```


```python
re.match('^...',my_string,re.I)
```




    <re.Match object; span=(0, 3), match='pyt'>




```python
re.match('^...',my_string,re.I).group()
```




    'pyt'




```python
# $ - dollar - The end of a sentence/string or line.
# my_string="python"
```


```python
re.match('...$',my_string,re.I) # no match
```


```python
re.match('...$',my_string,re.I).group() # no match
```


    ---------------------------------------------------------------------------

    AttributeError                            Traceback (most recent call last)

    <ipython-input-37-133687e801fe> in <module>
    ----> 1 re.match('...$',my_string,re.I).group() # no match
    

    AttributeError: 'NoneType' object has no attribute 'group'



```python
print (re.search('...$',my_string,re.I))
```

    <re.Match object; span=(3, 6), match='hon'>
    


```python
re.search('...$',my_string,re.I).group()
```




    'hon'




```python
## exercise
# i want to invite friends who names are exactly 5 characters

## grep -i emulex file.txt
# ex: emulex1,emulex2,emulex2,heademulex
## grep -iw emulex file.txt
# ex: emulex
```


```python
my_friends = ['vikram','santosh','kumar','mounika','narayana','phani']
```


```python
import re
for friend in my_friends:
    if re.search('^.....$',friend):
        print ("you are allowed to the party - {}".format(re.search('^.....$',friend).group()))
```

    you are allowed to the party - kumar
    you are allowed to the party - phani
    


```python
# globbling characters/greedy characters
# glob
```


```python
# * -> It represents zero or more characters.
# + -> It represents one or more characters.
# ? -> it represents zero or one characters.
```


```python
my_film1 = "ashique"
my_film2 = "aashique"
my_film3 = "aaashique"
my_film4 = "shique"
```


```python
# *
# a* - a repeated zero or more time
re.search('a*shique',my_film1) 
```




    <re.Match object; span=(0, 7), match='ashique'>




```python
re.search('a*shique',my_film2)
```




    <re.Match object; span=(0, 8), match='aashique'>




```python
re.search('a*shique',my_film3)
```




    <re.Match object; span=(0, 9), match='aaashique'>




```python
re.search('a*shique',my_film4) # shique
```




    <re.Match object; span=(0, 6), match='shique'>




```python
# +
# a+ -> a is repeated one or more time
```
my_film1 = "ashique"
my_film2 = "aashique"
my_film3 = "aaashique"
my_film4 = "shique"

```python
re.search('a+shique',my_film1)
```




    <re.Match object; span=(0, 7), match='ashique'>




```python
re.search('a+shique',my_film2)
```




    <re.Match object; span=(0, 8), match='aashique'>




```python
re.search('a+shique',my_film3)
```




    <re.Match object; span=(0, 9), match='aaashique'>




```python
re.search('a+shique',my_film4) # no match
```
my_film1 = "ashique"
my_film2 = "aashique"
my_film3 = "aaashique"
my_film4 = "shique"

```python
# ? 
# a? -? a repeated zero or one time
```


```python
re.match('a?shique',my_film1)
```




    <re.Match object; span=(0, 7), match='ashique'>




```python
re.match('a?shique',my_film2) # no match
```


```python
re.search('a?shique',my_film1)
```




    <re.Match object; span=(0, 7), match='ashique'>




```python
re.match('a?shique',my_film3)
```


```python
re.search('a?shique',my_film1)
```




    <re.Match object; span=(0, 7), match='ashique'>




```python
re.match('a?shique',my_film4)
```




    <re.Match object; span=(0, 6), match='shique'>




```python
re.search('a?shique',my_film1)
```




    <re.Match object; span=(0, 7), match='ashique'>




```python
# example on globbling.
# greedy characters - maximal matching by default
# how to go for minimal matching - *?,+?,??
```


```python
#html,yaml,json
my_string = "<h1>hello guys</h1>"  # <h1> -> minimal , <h1>hello guys</h1> -> maximal
my_string1 = "<hello>whats up</hello>"
my_string2 = "<whatsup> all is great </whatsup>"
my_string3 = "<howdy> what are you doing </howdy>"
```


```python
import re
```


```python
# maximal matching
re.match('<.*>',my_string)
```




    <re.Match object; span=(0, 19), match='<h1>hello guys</h1>'>




```python
# minimal matching
re.match('<.*?>',my_string)
```




    <re.Match object; span=(0, 4), match='<h1>'>




```python
# 2.x
re.match('<.*?>',my_string).group()
```




    '<h1>'




```python
# compile
help(re.compile)
```

    Help on function compile in module re:
    
    compile(pattern, flags=0)
        Compile a regular expression pattern, returning a Pattern object.
    
    


```python
# reg is any variable
reg = re.compile('<.*?>',re.I)
```


```python
print (reg,type(reg))
```

    re.compile('<.*?>', re.IGNORECASE) <class 're.Pattern'>
    


```python
dir(reg)
```




    ['__class__',
     '__copy__',
     '__deepcopy__',
     '__delattr__',
     '__dir__',
     '__doc__',
     '__eq__',
     '__format__',
     '__ge__',
     '__getattribute__',
     '__gt__',
     '__hash__',
     '__init__',
     '__init_subclass__',
     '__le__',
     '__lt__',
     '__ne__',
     '__new__',
     '__reduce__',
     '__reduce_ex__',
     '__repr__',
     '__setattr__',
     '__sizeof__',
     '__str__',
     '__subclasshook__',
     'findall',
     'finditer',
     'flags',
     'fullmatch',
     'groupindex',
     'groups',
     'match',
     'pattern',
     'scanner',
     'search',
     'split',
     'sub',
     'subn']




```python
help(reg.match)
```

    Help on built-in function match:
    
    match(string, pos=0, endpos=9223372036854775807) method of re.Pattern instance
        Matches zero or more characters at the beginning of the string.
    
    


```python
help(re.match)
```

    Help on function match in module re:
    
    match(pattern, string, flags=0)
        Try to apply the pattern at the start of the string, returning
        a Match object, or None if no match was found.
    
    
# without compile
re.match('<.*?>',my_string)
re.match('<.*?>',my_string1)
re.match('<.*?>',my_string2)
re.match('<.*?>',my_string3)

```python
# with compile
reg = re.compile('<.*?>',re.I) # minimal matching
reg1 = re.compile('<.*>',re.I) # maximal matching
```


```python
reg.match(my_string)
```




    <re.Match object; span=(0, 4), match='<h1>'>




```python
reg.match(my_string1)
```




    <re.Match object; span=(0, 7), match='<hello>'>




```python
reg.match(my_string2)
```




    <re.Match object; span=(0, 9), match='<whatsup>'>




```python
reg.match(my_string3)
```




    <re.Match object; span=(0, 7), match='<howdy>'>




```python
## anchors
```
{m} -> search for a pattern m number of times.
{m,n} -> search for a pattern matching between m to n number of times.
{m,} -> search for a pattern m and more number of times

```python
my_film1 = "ashique"
my_film2 = "aashique"
my_film3 = "aaashique"
my_film4 = "shique"
```


```python
reg = re.compile('a{2,}shique',re.I)
```


```python
print (reg)
```

    re.compile('a{2,}shique', re.IGNORECASE)
    


```python
reg.match(my_film1) # re.match('a{2,}shique',my_film1,re.I)
```


```python
reg.match(my_film2)
```




    <re.Match object; span=(0, 8), match='aashique'>




```python
reg.match(my_film3)
```




    <re.Match object; span=(0, 9), match='aaashique'>




```python
reg.match(my_film4)
```


```python
## character set.
```
[a-z] -> Matches any character from a to z.
[0-9] -> Matches any character from 0 to 9.
^[a-z] -> Does not match characters from a to z.
[^a-z] -> Matches characters beginning from a to z.
[*.?+] -> All the re pattern behave as symbols.

```python
my_string = "Python is a good language."
```


```python
import re
```


```python
# [a-z] -> character set
# + -> globbling character
# \s-> character set.
re.match('[a-z]+\s+[a-z]+\s+[a-z]+\s+[a-z]+\s+[a-z]+',my_string,re.I).group()
```




    'Python is a good language'




```python
# time to use anchors
# we have to take care of symbol.
# \s+ -> [ \t\n\r\f\v]
# \w+ -> [a-zA-Z0-9_]
re.match('([a-z]+\s+){4}[a-z]+[.]',my_string,re.I).group()
```




    'Python is a good language.'




```python
# \s+ -> [ \t\n\r\f\v]
# \w+ -> [a-zA-Z0-9_]
re.match('(\w+\s+){4}\w+[.]',my_string,re.I).group()
```




    'Python is a good language.'




```python
## verbose - VERBOSE OR re.X
re.match(
'''(\w+\s+)          # a word and a space
{4}                  # similar words like word and space having count of 4
\w+                  # A word
[.]                  # Followed by a full stop.
''',my_string,re.I|re.X).group()
```




    'Python is a good language.'




```python
## grouping
```


```python
my_string = "Python is a good language."
re.match('[a-z]+\s+[a-z]+\s+[a-z]+\s+[a-z]+\s+[a-z]+',my_string,re.I)
```




    <re.Match object; span=(0, 25), match='Python is a good language'>




```python
# when there is a regular expression match - what matched ? group.
re.match('[a-z]+\s+[a-z]+\s+[a-z]+\s+[a-z]+\s+[a-z]+',my_string,re.I).group()
```




    'Python is a good language'




```python
## grouping using indexing
```


```python
# group() and group(0) are all same
re.match('[a-z]+\s+[a-z]+\s+[a-z]+\s+[a-z]+\s+[a-z]+',my_string,re.I).group(0)
```




    'Python is a good language'




```python
# i need python and language as my values.
re.match('([a-z]+)\s+[a-z]+\s+[a-z]+\s+[a-z]+\s+([a-z]+)',my_string,re.I).group()
```




    'Python is a good language'




```python
re.match('([a-z]+)\s+[a-z]+\s+[a-z]+\s+[a-z]+\s+([a-z]+)',my_string,re.I).group(1)
```




    'Python'




```python
re.match('([a-z]+)\s+[a-z]+\s+[a-z]+\s+[a-z]+\s+([a-z]+)',my_string,re.I).group(2)
```




    'language'




```python
re.match('([a-z]+)\s+[a-z]+\s+[a-z]+\s+[a-z]+\s+([a-z]+)',my_string,re.I).group(1,2)
```




    ('Python', 'language')




```python
re.match('([a-z]+)\s+[a-z]+\s+[a-z]+\s+[a-z]+\s+([a-z]+)',my_string,re.I).groups()
```




    ('Python', 'language')




```python
# grouping using key based.
```


```python
re.match('(?P<snake>[a-z]+)\s+[a-z]+\s+[a-z]+\s+[a-z]+\s+(?P<latin>[a-z]+)',my_string,re.I).group()
```




    'Python is a good language'




```python
re.match('(?P<snake>[a-z]+)\s+[a-z]+\s+[a-z]+\s+[a-z]+\s+(?P<latin>[a-z]+)',my_string,re.I).group('snake')
```




    'Python'




```python
re.match('(?P<snake>[a-z]+)\s+[a-z]+\s+[a-z]+\s+[a-z]+\s+(?P<latin>[a-z]+)',my_string,re.I).group('latin')
```




    'language'




```python
re.match('(?P<snake>[a-z]+)\s+[a-z]+\s+[a-z]+\s+[a-z]+\s+(?P<latin>[a-z]+)',my_string,re.I).group('snake','latin')
```




    ('Python', 'language')




```python
re.match('(?P<snake>[a-z]+)\s+[a-z]+\s+[a-z]+\s+[a-z]+\s+(?P<latin>[a-z]+)',my_string,re.I).groups()
```




    ('Python', 'language')




```python
## multiline and findall
```


```python
my_string = "python is my love.\nPython is one of the greatest languages.\nPython is english line."
```


```python
my_string
```




    'python is my love.\nPython is one of the greatest languages.\nPython is english line.'




```python
print (my_string)
```

    python is my love.
    Python is one of the greatest languages.
    Python is english line.
    


```python
## match
re.match('python',my_string) # python is matched. 
```




    <re.Match object; span=(0, 6), match='python'>




```python
## search
re.search('python',my_string) # python is searched.
```




    <re.Match object; span=(0, 6), match='python'>




```python
# findall
help(re.findall)
```

    Help on function findall in module re:
    
    findall(pattern, string, flags=0)
        Return a list of all non-overlapping matches in the string.
        
        If one or more capturing groups are present in the pattern, return
        a list of groups; this will be a list of tuples if the pattern
        has more than one group.
        
        Empty matches are included in the result.
    
    


```python
re.findall("python",my_string,re.I)
```




    ['python', 'Python', 'Python']




```python
# lets me find for python in beginnning of each sentence
# 'python is my love.\nPython is one of the greatest languages.\nPython is english line.'
re.findall("^python",my_string,re.I)
```




    ['python']




```python
# re.M or re.MULTILINE
```




    ['python', 'Python', 'Python']


python is my love.
Python is one of the greatest languages.
Python is english line.

```python
re.findall("^python",my_string,re.I|re.M)
```




    ['python', 'Python', 'Python']




```python

```
