#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  4 18:31:59 2019

@author: tuxfux
# break - breaks us of the loop.
# continue - skips the iteration.
# pass  - does nothing
# exit  - kicks us out the program.

# kumar - not come to exam
"""
absentee=["kumar","vikram","phani"]
for student in ["bendu","kishan","kumar","vikram","mounika","phani"]:
    if student in absentee:
        continue
        #break
        #pass
    print ("results of {}".format(student))
