#!/usr/bin/python
# -*- coding: utf-8 -*-
# logging.basicconfig ?
# 
'''
The default level is WARNING, 
which means that only events of this level and above will be tracked,
 unless the logging package is configured to do otherwise.
'''

import logging as l
l.debug("This is a debug message")
l.info("This is a information message")
l.warning("This is a warning message")
l.error("This is an error message")
l.critical("This is a critical message")

'''
output
WARNING:root:This is a warning message
ERROR:root:This is an error message
CRITICAL:root:This is a critical message
'''

