#!/usr/bin/python
# -*- coding: utf-8 -*-
# logging.basicConfig?
# logging.Formatter?
# http://man7.org/linux/man-pages/man1/date.1.html - for datefmt
# time.strftime().
# linux - crontab
# windows - scheduler

import logging
logging.basicConfig(
        filename="myfirstlog.txt",
        filemode="a",
        level=logging.DEBUG,
        format="%(asctime)s - %(levelname)s - %(name)s - %(message)s",
        datefmt='%c')

# Main
disk_size = int(input("what is the disk size:"))

if disk_size < 50:
    logging.info("My disk is healthy and hale - {}".format(disk_size))
elif disk_size < 70:
    logging.warning("We are heading towards a disk error - {}".format(disk_size))
elif disk_size < 80:
    logging.error("Guys my disk is full - {}".format(disk_size))
else:
    logging.critical("The application has gone sleeping mode - {}".format(disk_size))
    
'''
* for a small team.
* its only writes to a file.
* basic purpose and not for enterprise level.

'''

