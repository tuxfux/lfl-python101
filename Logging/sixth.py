#!/usr/bin/python
# -*- coding: utf-8 -*-
# logging.basicConfig?
# logging.Formatter?
# http://man7.org/linux/man-pages/man1/date.1.html - for datefmt
# time.strftime().
# linux - crontab
# windows - scheduler
'''
Loggers expose the interface that application code directly uses.
ex: logging
logger -> root
logger -> gateway where all your message go.

Handlers send the log records (created by loggers) to the appropriate destination.
handler - filename,filemode
#        filename="myfirstlog.txt",
#        filemode="a",
handlers: https://docs.python.org/3/howto/logging.html#useful-handlers

Filters provide a finer grained facility for determining which log records to output.
ex:
level=logging.DEBUG - i want log messages from DEBUG and above.
# filters can be set both at logger level and also at handler level.

Formatters specify the layout of log records in the final output.
ex: format="%(asctime)s - %(levelname)s - %(name)s - %(message)s"

'''


import logging
#logging.basicConfig(
#        filename="myfirstlog.txt",
#        filemode="a",
#        level=logging.DEBUG,
#        format="%(asctime)s - %(levelname)s - %(name)s - %(message)s",
#        datefmt='%c')

#create logger
logger = logging.getLogger('Disk') # logger is a variable name.
logger.setLevel(logging.DEBUG)               # filter for the logger

# create console handler and set level to debug
ch = logging.FileHandler(filename='myfirstlog.txt') # one of the handler
ch.setLevel(logging.DEBUG)   # filter for the handler

# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

## Marry logger and handler
# add formatter to ch
ch.setFormatter(formatter) # handler and formatter
# add ch to logger
logger.addHandler(ch)      # logger and handler

# Main
disk_size = int(input("what is the disk size:"))

if disk_size < 50:
    logger.info("My disk is healthy and hale - {}".format(disk_size))
elif disk_size < 70:
    logger.warning("We are heading towards a disk error - {}".format(disk_size))
elif disk_size < 80:
    logger.error("Guys my disk is full - {}".format(disk_size))
else:
    logger.critical("The application has gone sleeping mode - {}".format(disk_size))
    


