#!/usr/bin/python
import logging

logger = logging.getLogger('simple_example') # logger
logger.setLevel(logging.ERROR)               # filter for logger
# create file handler which logs even debug messages
fh = logging.FileHandler('spam.log')         # file handler.
fh.setLevel(logging.DEBUG)                   # filter for handler.
# create console handler with a higher log level
ch = logging.StreamHandler()                 # stream handler
ch.setLevel(logging.ERROR)                   # filter for handler.
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
# add the handlers to logger
logger.addHandler(ch)
logger.addHandler(fh)

# 'application' code
logger.debug('debug message')
logger.info('info message')
logger.warning('warn message')
logger.error('error message')
logger.critical('critical message')