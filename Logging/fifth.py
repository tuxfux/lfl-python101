#!/usr/bin/python
# -*- coding: utf-8 -*-
# we can set filter at both logger and handler level.
# first priority is the logger filter.
# second priority is given to the handler filter.

import logging

# create logger
logger = logging.getLogger('Disk') # logger is a variable name.
logger.setLevel(logging.DEBUG)               # filter for the logger

# create console handler and set level to debug
ch = logging.StreamHandler() # one of the handler
ch.setLevel(logging.DEBUG)   # filter for the handler

# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

## Marry logger and handler
# add formatter to ch
ch.setFormatter(formatter) # handler and formatter
# add ch to logger
logger.addHandler(ch)      # logger and handler

# 'application' code
logger.debug('debug message')
logger.info('info message')
logger.warning('warn message')
logger.error('error message')
logger.critical('critical message')