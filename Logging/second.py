#!/usr/bin/python
# -*- coding: utf-8 -*-
# logging.basicConfig?
# logging.Formatter?
# http://man7.org/linux/man-pages/man1/date.1.html - for datefmt

import logging
logging.basicConfig(
        filename="myfirstlog.txt",
        filemode="a",
        level=logging.DEBUG,
        format="%(asctime)s - %(levelname)s - %(name)s - %(message)s",
        datefmt='%c')

# Main

logging.debug("This is a debug message")
logging.info("This is a information message")
logging.warning("This is a warning message")
logging.error("This is an error message")
logging.critical("This is a critical message")
