#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Sat Oct  5 18:06:06 2019

@author: tuxfux
"""

days = ["yesterday","today","tomorrow","dayafter"]

# task1
#output
#yesterday  9
#today      5
#tomorrow   8
#dayafter   8
'''
for value in days:
    print ("{} {}".format(value,len(value)))
'''

# task2
# output
# Yesterday
# TOday
# TOMorrow
# DAYAfter

'''
my_string="python"

my_string[0:3]
Out[22]: 'pyt'

my_string[:3]
Out[23]: 'pyt'

my_string[3:]
Out[24]: 'hon'

my_string[:2]
Out[25]: 'py'

my_string[2:]
Out[26]: 'thon'
'''

for value in days:
    #print ("{} {}".format((days.index(value)),(value[days.index(value)])))
    #print ("{} {}".format((days.index(value)),(value[0:days.index(value) + 1])))
    # 
    print (value[:days.index(value) + 1].upper() + value[days.index(value) + 1:] ) 
    #   my_string[:3]                            + my_string[3:]