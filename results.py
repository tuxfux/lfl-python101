#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  4 18:31:59 2019

@author: tuxfux
# break - breaks us of the loop.
# continue - skips the iteration.
# pass  - does nothing
# exit  - kicks us out the program.

# kumar - not come to exam
"""

for student in "bendu","kishan","kumar","mounika","phani":
    if student == "kumar":
        continue
        #break
        #pass
    print ("results of {}".format(student))
