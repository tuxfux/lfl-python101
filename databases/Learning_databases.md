# Database
databases are basically for storing your data and quick retrival of the data.

Python with database
+ relation databases  - mariabdb,oracle,mysql,postgres
+ non-relations databases - mongodb,cassendra,cowchdb# Database setup
# mariadb/mysql - MW - 2009 sun microsystems <- mysql(2007) -> oracle
# EU -> GPL -> 2016(mysql will be opensource)
# mariadb -> ubuntu/google## datasetup
## mysql

1) Installation
sudo apt-get install mysql-server

## swith to root user and try hitting mysql to login to the mysql promptmysql> help contents;
You asked for help about help category: "Contents"
For more information, type 'help <item>', where <item> is one of the following
categories:
   Account Management
   Administration
   Compound Statements
   Contents
   Data Definition
   Data Manipulation
   Data Types
   Functions
   Geographic Features
   Help Metadata
   Language Structure
   Plugins
   Procedures
   Storage Engines
   Table Maintenance
   Transactions
   User-Defined Functions
   Utility

mysql>
root@LFL:~# mysql
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 5
Server version: 5.7.27-0ubuntu0.19.04.1 (Ubuntu)

Copyright (c) 2000, 2019, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql>
mysql> help Functions;
You asked for help about help category: "Functions"
For more information, type 'help <item>', where <item> is one of the following
categories:
   Bit Functions
   Comparison Operators
   Control Flow Functions
   Date and Time Functions
   Encryption Functions
   GROUP BY Functions and Modifiers
   Information Functions
   Locking Functions
   Logical Operators
   Miscellaneous Functions
   Numeric Functions
   Spatial Functions
   String Functions

mysql> help Information Functions;
You asked for help about help category: "Information Functions"
For more information, type 'help <item>', where <item> is one of the following
topics:
   BENCHMARK
   CHARSET
   COERCIBILITY
   COLLATION
   CONNECTION_ID
   CURRENT_USER
   DATABASE
   FOUND_ROWS
   LAST_INSERT_ID
   ROW_COUNT
   SCHEMA
   SESSION_USER
   SYSTEM_USER
   USER
   VERSION

mysql> help VERSION;
Name: 'VERSION'
Description:
Syntax:
VERSION()

Returns a string that indicates the MySQL server version. The string
uses the utf8 character set. The value might have a suffix in addition
to the version number. See the description of the version system
variable in
https://dev.mysql.com/doc/refman/5.7/en/server-system-variables.html.

URL: https://dev.mysql.com/doc/refman/5.7/en/information-functions.html

Examples:
mysql> SELECT VERSION();
        -> '5.7.28-standard'

mysql> select version();
+-------------------------+
| version()               |
+-------------------------+
| 5.7.27-0ubuntu0.19.04.1 |
+-------------------------+
1 row in set (0.00 sec)

mysql>
mysql> help database;
Name: 'DATABASE'
Description:
Syntax:
DATABASE()

Returns the default (current) database name as a string in the utf8
character set. If there is no default database, DATABASE() returns
NULL. Within a stored routine, the default database is the database
that the routine is associated with, which is not necessarily the same
as the database that is the default in the calling context.

URL: https://dev.mysql.com/doc/refman/5.7/en/information-functions.html

Examples:
mysql> SELECT DATABASE();
        -> 'test'

mysql> select database();
+------------+
| database() |
+------------+
| NULL       |
+------------+
1 row in set (0.00 sec)

mysql>
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
4 rows in set (0.01 sec)

mysql>
mysql> help create;
Many help items for your request exist.
To make a more specific request, please type 'help <item>',
where <item> is one of the following
topics:
   CREATE DATABASE
   CREATE EVENT
   CREATE FUNCTION
   CREATE FUNCTION UDF
   CREATE INDEX
   CREATE LOGFILE GROUP
   CREATE PROCEDURE
   CREATE SCHEMA
   CREATE SERVER
   CREATE TABLE
   CREATE TABLESPACE
   CREATE TRIGGER
   CREATE USER
   CREATE VIEW
   SHOW
   SHOW CREATE DATABASE
   SHOW CREATE EVENT
   SHOW CREATE FUNCTION
   SHOW CREATE PROCEDURE
   SHOW CREATE SCHEMA
   SHOW CREATE TABLE
   SHOW CREATE USER
   SPATIAL INDEXES
## TASK1: CREATE A DATABASE TO PLAY AROUND.

mysql> help create database;
Name: 'CREATE DATABASE'
Description:
Syntax:
CREATE {DATABASE | SCHEMA} [IF NOT EXISTS] db_name
    [create_specification] ...

create_specification:
    [DEFAULT] CHARACTER SET [=] charset_name
  | [DEFAULT] COLLATE [=] collation_name

CREATE DATABASE creates a database with the given name. To use this
statement, you need the CREATE privilege for the database. CREATE
SCHEMA is a synonym for CREATE DATABASE.

URL: https://dev.mysql.com/doc/refman/5.7/en/create-database.html


mysql> create database lfl101;
Query OK, 1 row affected (0.00 sec)

## TASK 2: create a user to access the database.
mysql> help CREATE USER;
Name: 'CREATE USER'
Description:
Syntax:
CREATE USER [IF NOT EXISTS]
    user [auth_option] [, user [auth_option]] ...
    [REQUIRE {NONE | tls_option [[AND] tls_option] ...}]
    [WITH resource_option [resource_option] ...]
    [password_option | lock_option] ...


mysql> create user 'user101'@'localhost' identified by 'user101';
Query OK, 0 rows affected (0.00 sec)

mysql>
## TASK 3: Grant priviledges to the user of our database.

mysql> help grant;
Name: 'GRANT'
Description:
Syntax:
GRANT
    priv_type [(column_list)]
      [, priv_type [(column_list)]] ...
    ON [object_type] priv_level
    TO user [auth_option] [, user [auth_option]] ...
    [REQUIRE {NONE | tls_option [[AND] tls_option] ...}]
    [WITH {GRANT OPTION | resource_option} ...]

mysql> grant all on lfl101.* to 'user101'@'localhost';
Query OK, 0 rows affected (0.00 sec)

mysql>
### welcome user

tuxfux@LFL:~$ mysql -u user101 -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 6
Server version: 5.7.27-0ubuntu0.19.04.1 (Ubuntu)

Copyright (c) 2000, 2019, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| lfl101             |
+--------------------+
2 rows in set (0.00 sec)

mysql> select database();
+------------+
| database() |
+------------+
| NULL       |
+------------+
1 row in set (0.00 sec)

mysql> use lfl101;
Database changed
mysql>
mysql> select database();
+------------+
| database() |
+------------+
| lfl101     |
+------------+
1 row in set (0.00 sec)

mysql> show tables;
Empty set (0.00 sec)

mysql>

## Installation of plugin to play with database in the background
sudo apt-get install python-mysqldb
##  my first program - fetch the version

#!/usr/bin/env python
import MySQLdb as mdb
conn = mdb.connect('localhost','user101','user101','lfl101')
#                  server,username,password,database
cur = conn.cursor()
cur.execute("select version()")
output = cur.fetchone()
print output[0].split('-')[0]
## my second program - creation of the table

#!/usr/bin/env python
import MySQLdb as mdb
conn = mdb.connect('localhost','user101','user101','lfl101')
cur = conn.cursor()
cur.execute('create table student (name varchar(10),gender varchar(6))')
conn.close()mysql> select version();
+-------------------------+
| version()               |
+-------------------------+
| 5.7.27-0ubuntu0.19.04.1 |
+-------------------------+
1 row in set (0.00 sec)

mysql> show tables;
ERROR 1046 (3D000): No database selected
mysql>
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| lfl101             |
+--------------------+
2 rows in set (0.00 sec)

mysql> use lfl101;
Database changed
mysql> show tables;
Empty set (0.00 sec)

mysql> show tables;
+------------------+
| Tables_in_lfl101 |
+------------------+
| student          |
+------------------+
1 row in set (0.00 sec)

mysql> desc student;
+--------+-------------+------+-----+---------+-------+
| Field  | Type        | Null | Key | Default | Extra |
+--------+-------------+------+-----+---------+-------+
| name   | varchar(10) | YES  |     | NULL    |       |
| gender | varchar(6)  | YES  |     | NULL    |       |
+--------+-------------+------+-----+---------+-------+
2 rows in set (0.01 sec)

mysql> show create table student;
+---------+---------------------------------------------------------------------------------------------------------------------------------------+
| Table   | Create Table                                                                                                                          |
+---------+---------------------------------------------------------------------------------------------------------------------------------------+
| student | CREATE TABLE `student` (
  `name` varchar(10) DEFAULT NULL,
  `gender` varchar(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 |
+---------+---------------------------------------------------------------------------------------------------------------------------------------+
1 row in set (0.00 sec)

## third.py

tuxfux@LFL:~/python/mysql$ cat third.py
#!/usr/bin/env python
import MySQLdb as mdb
conn = mdb.connect('localhost','user101','user101','lfl101')
cur = conn.cursor()
name = raw_input("Please enter your name:")
gender = raw_input("please enter your gender:")
cur.executemany("insert into student(name,gender) values (%s,%s)",[(name,gender)])
conn.commit()
conn.close()
tuxfux@LFL:~/python/mysql$
mysql> select * from student;
Empty set (0.00 sec)

mysql> show create table student;
+---------+-------------------------------------------------------------------------                                                                         --------------------------------------------------------------+
| Table   | Create Table                                                                                                                                                                                                   |
+---------+-------------------------------------------------------------------------                                                                         --------------------------------------------------------------+
| student | CREATE TABLE `student` (
  `name` varchar(10) DEFAULT NULL,
  `gender` varchar(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 |
+---------+-------------------------------------------------------------------------                                                                         --------------------------------------------------------------+
1 row in set (0.00 sec)

mysql> show engines;
+--------------------+---------+----------------------------------------------------------------+--------------+------+------------+
| Engine             | Support | Comment                                                        | Transactions | XA   | Savepoints |
+--------------------+---------+----------------------------------------------------------------+--------------+------+------------+
| InnoDB             | DEFAULT | Supports transactions, row-level locking, and foreign keys     | YES          | YES  | YES        |
| MRG_MYISAM         | YES     | Collection of identical MyISAM tables                          | NO           | NO   | NO         |
| MEMORY             | YES     | Hash based, stored in memory, useful for temporary tables      | NO           | NO   | NO         |
| BLACKHOLE          | YES     | /dev/null storage engine (anything you write to it disappears) | NO           | NO   | NO         |
| MyISAM             | YES     | MyISAM storage engine                                          | NO           | NO   | NO         |
| CSV                | YES     | CSV storage engine                                             | NO           | NO   | NO         |
| ARCHIVE            | YES     | Archive storage engine                                         | NO           | NO   | NO         |
| PERFORMANCE_SCHEMA | YES     | Performance Schema                                             | NO           | NO   | NO         |
| FEDERATED          | NO      | Federated MySQL storage engine                                 | NULL         | NULL | NULL       |
+--------------------+---------+----------------------------------------------------------------+--------------+------+------------+
9 rows in set (0.00 sec)

mysql> select * from student;
+--------+--------+
| name   | gender |
+--------+--------+
| vikram | male   |
+--------+--------+
1 row in set (0.00 sec)

## company
CEO1 meets another CEO2 ( 2weeks times - postgres)

CEO1
|
|
clients
|
|
webapi ( used by our clients) - 1 days
|
|
middleware ( this is code base/raw database commands) - 5 days
|
|
database ( postgre)   - 1 days
 * creates a database/schema
 * creates a user
 * grants access to the user on schema
 * I will pump the data from mysql tables to postgre tables.
 
 2010 - google took exactly 5 days to move the data from mysql to mariadb# sqlalchemy
https://www.sqlalchemy.org/
https://www.slideshare.net/PostgresOpen/michael-bayer-introduction-to-sqlalchemy ( page 9)
https://www.youtube.com/watch?v=P141KRbxVKc

```python

```
