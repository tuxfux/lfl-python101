#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Sat Oct  5 17:35:28 2019

@author: tuxfux
"""
# Input
my_fruits = ['apple','apple','banana','banana','cherry']
#dupli_fruits = my_fruits # softcopy - wrong
dupli_fruits = my_fruits[:] # deepcopy - right
#my_fruits = ['apple','banana','apple','banana','banana','cherry','cherry','cherry']
#my_fruits = ['apple','apple','apple','apple','banana','banana','cherry']

# output
# my_dupli = ['apple','banana']
# my_fruits = ['apple','banana','cherry']

my_dupli = []
#for value in dupli_fruits:
for value in my_fruits[:]:
    if my_fruits.count(value) > 1:
        if value not in my_dupli:
            my_dupli.append(value)
        my_fruits.remove(value)

print (my_dupli)
print (my_fruits)    

'''
first
my_fruits = ['apple','apple','apple','apple','banana','banana','cherry']
my_fruits[:] = ['apple','apple','apple','apple','banana','banana','cherry']
second
my_fruits = ['apple','apple','apple','banana','banana','cherry']
third
my_fruits = ['apple','apple','banana','banana','cherry']
fourth
my_fruits = ['apple','apple','banana','cherry']
my_dupli=['apple']

'''    