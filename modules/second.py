#!/usr/bin/python
# -*- coding: utf-8 -*-
# my_add is a function within local namespace.
# f.my_add is a function from namespace of my_add

import sys
sys.path.insert(0,'C:\\Users\\tuxfux\\Documents\\lfl-python101\\modules\\extra')

import first as f

def my_add(a,b):
    ''' this is for addition of string '''
    a = int(a)
    b = int(b)
    return a + b

# MAIN
if __name__ == '__main__':
    print ("Addition of two numbers is {}".format(my_add(10,20)))
    print ("Addition of two string is {}".format(f.my_add("cloud"," burst")))
    
'''
* you have to import folder from different locations.
ex: extra,extra1,extra2,extra3,extra4 folders
* if i have a file with similar names in all folders.

extra
 - first.py
extra1
 - first.py
extra2
 - first.py
extra3
 - first.py
 
solution: packages

'''

