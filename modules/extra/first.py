#!/usr/bin/python
# -*- coding: utf-8 -*-

version = 1.0

def my_add(a,b):
    ''' This is for addition of numbers - int and str  '''
    return a + b

def my_sub(a,b):
    ''' This is for substraction of numbers - sub '''
    return a - b

def my_multi(a,b):
    ''' This is for multiplication of numbers - multi '''
    return a * b

def my_div(a,b):
    ''' This is for division of the numbers - div '''
    return a/b

# Main
if __name__ == '__main__':
    print ("Initiating the bomb")