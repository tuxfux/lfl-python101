#!/usr/bin/env python
# -*- coding: utf-8 -*-

def first_lin1_fun1():
    return ("This is my first lin1 function")

def second_lin1_fun1():
    return ("This is my second lin1 function")

def third_lin1_fun1():
    return ("This is my third lin1 function")

def fourth_lin1_fun2():
    return ("This is my fourth lin1 function")

## Main
if __name__ == '__main__':
    print ("This is my first function")
