#!/usr/bin/env python
# -*- coding: utf-8 -*-

def first_lin4_fun1():
    return ("This is my first lin4 function")

def second_lin4_fun1():
    return ("This is my second lin4 function")

def third_lin4_fun1():
    return ("This is my third lin4 function")

def fourth_lin4_fun2():
    return ("This is my fourth lin4 function")

## Main
if __name__ == '__main__':
    print ("This is my first function")

