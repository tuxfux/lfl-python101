#!/usr/bin/env python
# -*- coding: utf-8 -*-

def first_lin3_fun1():
    return ("This is my first lin3 function")

def second_lin3_fun1():
    return ("This is my second lin3 function")

def third_lin3_fun1():
    return ("This is my third lin3 function")

def fourth_lin3_fun2():
    return ("This is my fourth lin3 function")

## Main
if __name__ == '__main__':
    print ("This is my first function")

