#!/usr/bin/env python
# -*- coding: utf-8 -*-

def first_lin2_fun1():
    return ("This is my first lin2 function")

def second_lin2_fun1():
    return ("This is my second lin2 function")

def third_lin2_fun1():
    return ("This is my third lin2 function")

def fourth_lin2_fun2():
    return ("This is my fourth lin2   function")

## Main
if __name__ == '__main__':
    print ("This is my first function")
