#!/usr/bin/env python
# -*- coding: utf-8 -*-

def first_sol4_fun1():
    return ("This is my first sol4 function")

def second_sol4_fun1():
    return ("This is my second sol4 function")

def third_sol4_fun1():
    return ("This is my third sol4 function")

def fourth_sol4_fun2():
    return ("This is my fourth sol4 function")

## Main
if __name__ == '__main__':
    print ("This is my first function")

