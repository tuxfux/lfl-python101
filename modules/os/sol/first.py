#!/usr/bin/env python
# -*- coding: utf-8 -*-

def first_sol1_fun1():
    return ("This is my first sol1 function")

def second_sol1_fun1():
    return ("This is my second sol1 function")

def third_sol1_fun1():
    return ("This is my third sol1 function")

def fourth_sol1_fun2():
    return ("This is my fourth sol1 function")

## Main
if __name__ == '__main__':
    print ("This is my first function")
