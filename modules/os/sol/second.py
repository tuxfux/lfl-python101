#!/usr/bin/env python
# -*- coding: utf-8 -*-

def first_sol2_fun1():
    return ("This is my first sol2 function")

def second_sol2_fun1():
    return ("This is my second sol2 function")

def third_sol2_fun1():
    return ("This is my third sol2 function")

def fourth_sol2_fun2():
    return ("This is my fourth sols2   function")

## Main
if __name__ == '__main__':
    print ("This is my first function")
