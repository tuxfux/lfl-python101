#!/usr/bin/env python
# -*- coding: utf-8 -*-

def first_sol3_fun1():
    return ("This is my first sol3 function")

def second_sol3_fun1():
    return ("This is my second sol3 function")

def third_sol3_fun1():
    return ("This is my third sol3 function")

def fourth_sol3_fun2():
    return ("This is my fourth sol3 function")

## Main
if __name__ == '__main__':
    print ("This is my first function")

