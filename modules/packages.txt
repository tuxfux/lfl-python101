# media player
## package
## https://docs.python.org/3/tutorial/modules.html#packages

2012
computer 
 - Media player (*.mp3)
                (*.mp4)
                (*youtube)
                
Mediaplayer/                          Top-level package
      __init__.py               Initialize the sound package
      formats/                  Subpackage for file format conversions
              __init__.py
              wavread.py
              wavwrite.py
              aiffread.py
              aiffwrite.py
              auread.py
              auwrite.py
              ...
      effects/                  Subpackage for sound effects
              __init__.py
              echo.py
              surround.py
              reverse.py
              ...
      filters/                  Subpackage for filters
              __init__.py
              equalizer.py
              vocoder.py
              karaoke.py

# Infra example

Os
 - lin
     + ifconfig.py
     + storage.py
 - aix
      + ifconfig.py
      + storage.py
 - hpux
      + ifconfig.py
      + storage.py
 - sol
      + ifconfig.py
      + storage.py
      
# 
Os.lin.ifconfig  # initiate the linux based ifconfig commands
Os.aix.ifconfig
Os.hpux.ifconfig
Os.sol.ifconfig



## packages

1. create a folder os.
2. under in create os/{lin,sol,aix,hpux}
3. under os/lin create some files first.py,second.py,third.py and fourth.py
4. switch to the location os
5. try to go to ipytyhon and import lin folder.

## 2.x

(base) C:\Users\tuxfux\Documents\lfl-python101\modules\os>ipython
Python 2.7.16 |Anaconda, Inc.| (default, Mar 14 2019, 15:42:17) [MSC v.1500 64 bit (AMD64)]
Type "copyright", "credits" or "license" for more information.

IPython 5.8.0 -- An enhanced Interactive Python.
?         -> Introduction and overview of IPython's features.
%quickref -> Quick reference.
help      -> Python's own help system.
object?   -> Details about 'object', use 'object??' for extra details.

In [1]: import lin
---------------------------------------------------------------------------
ImportError                               Traceback (most recent call last)
<ipython-input-1-cb672e599c2e> in <module>()
----> 1 import lin

ImportError: No module named lin

In [2]: pwd
Out[2]: u'C:\\Users\\tuxfux\\Documents\\lfl-python101\\modules\\os'

In [3]:   

# 3.x

(base) C:\Users\tuxfux\Documents\lfl-python101\modules\os>ipython
Python 3.7.3 (default, Apr 24 2019, 15:29:51) [MSC v.1915 64 bit (AMD64)]
Type 'copyright', 'credits' or 'license' for more information
IPython 7.6.1 -- An enhanced Interactive Python. Type '?' for help.

In [1]: import lin

In [2]: dir(lin)
Out[2]:
['__doc__',
 '__file__',
 '__loader__',
 '__name__',
 '__package__',
 '__path__',
 '__spec__']

In [3]: pwd
Out[3]: 'C:\\Users\\tuxfux\\Documents\\lfl-python101\\modules\\os'

In [4]:                                                                                                                                                                                                                                                           

6. Now i want to access the files underneath of os/lin and also the funtions
underlying the files.

we have to create a package.
under your lin folder : 
- os/lin
  + __init__.py
    import first
    import second
    import third
    import fourth
    
Note: your __init__.py is the first file which get read/imported when you import
a folder.

7. Now try importing the lin folder and see what happens

# 2.x or 3.x

(base) C:\Users\tuxfux\Documents\lfl-python101\modules\os>ipython
Python 2.7.16 |Anaconda, Inc.| (default, Mar 14 2019, 15:42:17) [MSC v.1500 64 bit (AMD64)]
Type "copyright", "credits" or "license" for more information.

IPython 5.8.0 -- An enhanced Interactive Python.
?         -> Introduction and overview of IPython's features.
%quickref -> Quick reference.
help      -> Python's own help system.
object?   -> Details about 'object', use 'object??' for extra details.

In [1]: pwd
Out[1]: u'C:\\Users\\tuxfux\\Documents\\lfl-python101\\modules\\os'

In [3]: ls
 Volume in drive C has no label.
 Volume Serial Number is 768D-9FEC

 Directory of C:\Users\tuxfux\Documents\lfl-python101\modules\os

20-10-2019  18:11    <DIR>          .
20-10-2019  18:11    <DIR>          ..
20-10-2019  18:11    <DIR>          aix
20-10-2019  18:11    <DIR>          hpux
20-10-2019  18:28    <DIR>          lin
20-10-2019  18:11    <DIR>          sol
               0 File(s)              0 bytes
               6 Dir(s)  416,938,029,056 bytes free

In [4]: import lin

In [5]: dir(lin)
Out[5]:
['__builtins__',
 '__doc__',
 '__file__',
 '__name__',
 '__package__',
 '__path__',
 'first',
 'fourth',
 'second',
 'third']

In [6]: lin.first.first_lin1_fun1()
Out[6]: 'This is my first lin1 function'

In [7]: lin.first.second_lin1_fun1()
Out[7]: 'This is my second lin1 function'

In [8]:                                               
                         

8. I copied all the files from os/lin folder and copied it to
os/sol folder.i modified the lin to sol.

+ sol/__init__.py
    import first
    import second
    import third

In [4]: pwd
Out[4]: u'C:\\Users\\tuxfux\\Documents\\lfl-python101\\modules\\os'

In [5]: ls
 Volume in drive C has no label.
 Volume Serial Number is 768D-9FEC

 Directory of C:\Users\tuxfux\Documents\lfl-python101\modules\os

20-10-2019  18:11    <DIR>          .
20-10-2019  18:11    <DIR>          ..
20-10-2019  18:11    <DIR>          aix
20-10-2019  18:11    <DIR>          hpux
20-10-2019  18:31    <DIR>          lin
20-10-2019  18:38    <DIR>          sol
               0 File(s)              0 bytes
               6 Dir(s)  416,938,668,032 bytes free

In [6]: import lin

In [7]: import sol

In [8]: lin.first.first_lin1_fun1()
Out[8]: 'This is my first lin1 function'

In [9]: sol.first.first_sol1_fun1()
Out[9]: 'This is my first sol1 function'

## android mobile phone

in sol, when i import the folder.
first,second,third are getting loaded implicitly.

import sol

In [10]: dir(sol)
Out[10]:
['__builtins__',
 '__doc__',
 '__file__',
 '__name__',
 '__package__',
 '__path__',
 'first',
 'second',
 'third']


## i have explicityly loaded my module for a given package.
In [11]: from sol import fourth

In [12]: dir(sol)
Out[12]:
['__builtins__',
 '__doc__',
 '__file__',
 '__name__',
 '__package__',
 '__path__',
 'first',
 'fourth',
 'second',
 'third']
 
 
 In new release you are fourth,fifth,sixth


## a package example - xml

In [4]: import sys

In [5]: sys.path
Out[5]:
['',
 'C:\\Users\\tuxfux\\Anaconda2\\Scripts',
 'C:\\Users\\tuxfux\\Anaconda2\\python27.zip',
 'C:\\Users\\tuxfux\\Anaconda2\\DLLs',
 'C:\\Users\\tuxfux\\Anaconda2\\lib',
 'C:\\Users\\tuxfux\\Anaconda2\\lib\\plat-win',
 'C:\\Users\\tuxfux\\Anaconda2\\lib\\lib-tk',
 'C:\\Users\\tuxfux\\Anaconda2',
 'C:\\Users\\tuxfux\\Anaconda2\\lib\\site-packages',
 'C:\\Users\\tuxfux\\Anaconda2\\lib\\site-packages\\win32',
 'C:\\Users\\tuxfux\\Anaconda2\\lib\\site-packages\\win32\\lib',
 'C:\\Users\\tuxfux\\Anaconda2\\lib\\site-packages\\Pythonwin',
 'C:\\Users\\tuxfux\\Anaconda2\\lib\\site-packages\\IPython\\extensions',
 'C:\\Users\\tuxfux\\.ipython']
 
 
# cd C:\\Users\\tuxfux\\Anaconda2\\Lib

(base) C:\Users\tuxfux\Anaconda2\Lib>cd xml

(base) C:\Users\tuxfux\Anaconda2\Lib\xml>dir
 Volume in drive C has no label.
 Volume Serial Number is 768D-9FEC

 Directory of C:\Users\tuxfux\Anaconda2\Lib\xml

25-09-2019  18:21    <DIR>          .
25-09-2019  18:21    <DIR>          ..
25-09-2019  18:21    <DIR>          dom
25-09-2019  18:21    <DIR>          etree
25-09-2019  18:21    <DIR>          parsers
25-09-2019  18:21    <DIR>          sax
02-03-2019  23:47               980 __init__.py
15-03-2019  02:17             1,061 __init__.pyc
               2 File(s)          2,041 bytes
               6 Dir(s)  416,934,023,168 bytes free

## Assignment
how to create a bundle out of a package
https://docs.python.org/2/distutils/
https://docs.python.org/2.7/distributing/index.html
https://docs.python.org/2.7/installing/index.htmls


## Installation of modules.

# pip install <modulename>
# easy_install <modulename>
# setuptools

module listing - https://pypi.org/

* choose a right module.
  - extensive support - The module that you picks is well support.
  - collaboration

## env - production
ubuntu,redhat - os + python

virtualenv
 create a folder and contain the packages to that folder.
 
## I am creating for windows.. you can google for other os.

## install 
# pip install virtualenv
# pip install virtualenvwrapper-win

outputs:


 (base) C:\Users\tuxfux\Documents\lfl-python101\modules>pip install virtualenv
DEPRECATION: Python 2.7 will reach the end of its life on January 1st, 2020. Please upgrade your Python as Python 2.7 won't be maintained after that date. A future version of pip will drop support for Python 2.7.
Collecting virtualenv
  Downloading https://files.pythonhosted.org/packages/89/66/786e0d6f61bd0612f431e19b016d1ae46f1cb8d21a80352cc6774ec876e3/virtualenv-16.7.6-py2.py3-none-any.whl (3.4MB)
     |UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU| 3.4MB 3.0MB/s
Installing collected packages: virtualenv
Successfully installed virtualenv-16.7.6

(base) C:\Users\tuxfux\Documents\lfl-python101\modules>pip install virtualenvwrapper-win
DEPRECATION: Python 2.7 will reach the end of its life on January 1st, 2020. Please upgrade your Python as Python 2.7 won't be maintained after that date. A future version of pip will drop support for Python 2.7.
Collecting virtualenvwrapper-win
  Downloading https://files.pythonhosted.org/packages/87/70/ccebe989b112c42814fee4c145cfdac8fbcb174074b04bf8250b3052544f/virtualenvwrapper_win-1.2.5-py2-none-any.whl
Requirement already satisfied: virtualenv in c:\users\tuxfux\anaconda2\lib\site-packages (from virtualenvwrapper-win) (16.7.6)
Installing collected packages: virtualenvwrapper-win
Successfully installed virtualenvwrapper-win-1.2.5

## create your virtualenv

(base) C:\Users\tuxfux\Documents\lfl-python101\modules>mkvirtualenv myenv
 C:\Users\tuxfux\Envs is not a directory, creating
New python executable in C:\Users\tuxfux\Envs\myenv\Scripts\python.exe
Installing setuptools, pip, wheel...
done.

(myenv) (base) C:\Users\tuxfux\Documents\lfl-python101\modules>

## i want to install a module in my virtualenv

(myenv) (base) C:\Users\tuxfux\Documents\lfl-python101\modules>pip freeze
DEPRECATION: Python 2.7 will reach the end of its life on January 1st, 2020. Please upgrade your Python as Python 2.7 won't be maintained after that date. A future version of pip will drop support for Python 2.7. More details about Python 2 support in pip, can be found at https://pip.pypa.io/en/latest/development/release-process/#python-2-support

(myenv) (base) C:\Users\tuxfux\Documents\lfl-python101\modules>pip install excel
DEPRECATION: Python 2.7 will reach the end of its life on January 1st, 2020. Please upgrade your Python as Python 2.7 won't be maintained after that date. A future version of pip will drop support for Python 2.7. More details about Python 2 support in pip, can be found at https://pip.pypa.io/en/latest/development/release-process/#python-2-support
Collecting excel
  Downloading https://files.pythonhosted.org/packages/9d/5e/1420669f433ca41315685fb9bdc6fe2869a6e525cb6483805f3f4c9d61ad/excel-1.0.0.tar.gz
Collecting xlrd
  Downloading https://files.pythonhosted.org/packages/b0/16/63576a1a001752e34bf8ea62e367997530dc553b689356b9879339cf45a4/xlrd-1.2.0-py2.py3-none-any.whl (103kB)
     |UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU| 112kB 1.2MB/s
Building wheels for collected packages: excel
  Building wheel for excel (setup.py) ... done
  Created wheel for excel: filename=excel-1.0.0-cp27-none-any.whl size=3019 sha256=d7f2340e64b825cbe5404ed0108c80e1564376552b0e39980b75dd0ab3783f72
  Stored in directory: C:\Users\tuxfux\AppData\Local\pip\Cache\wheels\19\76\e3\f46c9f0b4594006300affc1b6477ddbb47d3aea96b1dedd9da
Successfully built excel
Installing collected packages: xlrd, excel
Successfully installed excel-1.0.0 xlrd-1.2.0

(myenv) (base) C:\Users\tuxfux\Documents\lfl-python101\modules>pip freeze
DEPRECATION: Python 2.7 will reach the end of its life on January 1st, 2020. Please upgrade your Python as Python 2.7 won't be maintained after that date. A future version of pip will drop support for Python 2.7. More details about Python 2 support in pip, can be found at https://pip.pypa.io/en/latest/development/release-process/#python-2-support
excel==1.0.0
xlrd==1.2.0

(myenv) (base) C:\Users\tuxfux\Documents\lfl-python101\modules> 

## Activate

(base) C:\Users\tuxfux\Documents\lfl-python101>C:\Users\tuxfux\Envs\myenv\Scripts\activate

(myenv) (base) C:\Users\tuxfux\Documents\lfl-python101>   

## deactivate

(myenv) (base) C:\Users\tuxfux\Documents\lfl-python101>C:\Users\tuxfux\Envs\myenv\Scripts\deactivate.bat

(base) C:\Users\tuxfux\Documents\lfl-python101>    


## 
export PYTHONPATH=$PYTHONPATH:<your_path> will give priority to system paths and only if something is not there will look on your path.

export PYTHONPATH=<your_path>:$PYTHONPATH will search <your_path> firstly and then $PYTHONPATH for what it does not find.

If something exists in both but you want to use one version of it for one program and the other for another then you might want to look on different bashrc profile          

+ /etc/profile
  + /etc/bashrc
    su - mounika
       .profile or .bash_profile
       .bashrc
       
       source .bashrc
       or 
       source .bashrc1
       
# first version
cat > myfirstversion
export PYTHONPATH=$PYTHONPATH:<your_path>
cat > mysecondversion
export PYTHONPATH=<your_path>:$PYTHONPATH
ex:
export /home/tuxfux/mynewenv/:#PYTHONPATH

source myfirstversion
source mysecondversion

usecase:

python -> /usr/bin/python 
python -> virtualenv -> /home/tuxfux/mynewenv/python
                                                                                                                                                                                                                                               