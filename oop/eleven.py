#!/usr/bin/python
# intro
# getters and setters
## i have a class where my function is to get the area of the two parameters.
## i can access lenght using the <instance>.attribute

## requirment
## 1. We need to read the length value.
## 2. I need to make sure that my lenght is not negative

class Area:
    def __init__(self,length=1):
        self.length = length
        
    def compute_area(self):
        my_area = self.length * self.length
        return (my_area)
    
    
## Main
A = Area(10)  # passed values to the constructor
print (A.length)
## modify the lenght
A.length = 20
print (A.length)
print (A.compute_area())
        


