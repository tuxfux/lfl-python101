#!/usr/bin/python
# -*- coding: utf-8 -*-
# constructor - __init__ - special method
# constructor is the first function which get called when you create an instance.

class Account(object):
    def __init__(self,amount=0):
        self.balance = amount
    def my_deposit(self,salary=1000):
        self.balance = self.balance + salary
    def my_withdraw(self,expense=200):
        self.balance = self.balance - expense
    def display_balance(self):
        return ("My balance is - {}".format(self.balance))
    
#Account.balance = 30000
#print (Account.balance)
 
## Main

if __name__ == '__main__':
## salary
    phani = Account()
    print ("balance of phani - {}".format(phani.balance))
    phani.my_deposit(5000)
    phani.my_withdraw(200)
    print (phani.display_balance())

## student
## Mounika
    mounika = Account(1000)
    print ("balance of mounika - {}".format(mounika.balance))
    mounika.my_deposit(10000)
    mounika.my_withdraw(8000)
    print (mounika.display_balance())
 
## entrepreneur
## bendu
    bendu = Account(5000)
    print ("Balance of bendu - {}".format(bendu.balance))
    bendu.my_deposit(20000)
    bendu.my_withdraw(25000)
    print (bendu.display_balance())

## see everyone balance    
    print ("balance of phani - {}".format(phani.balance))
    print ("balance of mounika - {}".format(mounika.balance))
    print ("Balance of bendu - {}".format(bendu.balance))

    
