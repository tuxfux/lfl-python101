#!/usr/bin/python

class Emp:
    def __init__(self,name,company):
        self._name = name        # protected
        self.__company = company # private
      
    @property
    def company(self):
        print ("calling the @getter function")
        return (self.__company)
        
    @company.setter
    def company(self,value):
        print ("calling the @setter function")
        self.__company = value
        
    
# main
santosh = Emp("santosh kumar","oracle")
#print (santosh.__company) # not possible
print (santosh.company)
print (santosh._name)
# carefull - dont do this with private variables
#santosh.__company = "BOA"
#print (santosh.__company)
santosh.company = "BOA"
print (santosh.company)
       