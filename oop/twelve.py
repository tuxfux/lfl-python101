#!/usr/bin/python
# getters and setters
## tomorrow

## requirment
## 1. We need to read the length value.
## 2. I need to make sure that my lenght is not negative

class Area:
    def __init__(self,length=1):
        self.__length = length # private
        
    def compute_area(self):
        my_area = self.__length * self.__length
        return (my_area)
    
    ## want to pring my length value
    ## one of solutions
#    def get_length(self):
#        return (self.__length)
    
    
    ## implemting the getter method
    @property
    def length(self):
        print ("Implementing the @getter function")
        return (self.__length)
    
#    ## implement the setter method
    @length.setter
    def length(self,value):
        print ("Implementing the @setter function")
        if value < 0:
            raise ValueError("Length cannot be less than Zero")
        self.__length = value
    
    
## Main
## case I
A = Area(10)
#print (A.__length)
#(base) C:\Users\tuxfux\Documents\lfl-python101\oop>python twelve.py
#Traceback (most recent call last):
#  File "twelve.py", line 33, in <module>
#    print (A.__length)
#AttributeError: 'Area' object has no attribute '__length'
#print (A.get_length())
print (A.length)  # calling my getter
print (A.compute_area()) # 100
A.length = 20
print (A.length)
print (A.compute_area()) #400
A.length = -200

## setters and getters


# case II
#A = Area(100)
#print (A.length)
#A.length = 200
#print (A.compute_area())

