#!/usr/bin/python
# -*- coding: utf-8 -*-
## @classmethods
## @staticmethods

class Student(object):
    def __init__(self,first_name,last_name):
        self.first_name = first_name
        self.last_name = last_name

    @classmethod
    def from_string(cls, name_str):
        first_name,last_name = name_str.split()
        student = cls(first_name,last_name)
        return student
    
    @staticmethod
    def is_full_name(name_str):
        names = name_str.split()
        return len(names) > 1
       
    def student_details(self):
            return ("The student details are - {} {}".format(self.first_name,self.last_name))
        
    
        
## Main
student1 = Student("phani","verma")
print (student1.student_details())

student2 = Student.from_string("Phani Verma")
print (student2.student_details())

## static function
print (Student.is_full_name("Mounika duddu"))
print (Student.is_full_name("kumar"))

