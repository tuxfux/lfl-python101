#!/usr/bin/python
## we defined a class/blueprint - Account
# balance -> data
# display_balance -> method


class Account:
    balance = 0                 # class variable
    def display_balance(self):  # method/function of class
        return ("my balance is {}".format(self.balance))
        
        
## Main
if __name__ == '__main__':
    print (Account)         # <class '__main__.Account'>
    print (type(Account))   # <class 'type'>
    print (Account())       # <__main__.Account object at 0x000002C8F68D3668>/instance
    print (type(Account())) # <class '__main__.Account'>
    phani = Account()       # instance of the Account class
    print (phani)           # <__main__.Account object at 0x00000209E59C3F28>/instance
    print (type(phani))     # <class '__main__.Account'>
    print (dir(phani))
    '''
    ['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', 
    '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__',
    '__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__', 
    '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__',
    '__str__', '__subclasshook__', '__weakref__', 'balance', 'display_balance']
    '''
    print (phani.balance) # 0
    #print (phani.display_balance()) # error
    '''
    Traceback (most recent call last):
    File "third.py", line 31, in <module>
    print (phani.display_balance())
    TypeError: display_balance() takes 0 positional arguments but 1 was given
    '''
## to take of the error i put self in the method defination
## self actually denotes the instance itself.
## self is not a keywork,its just a variable
    #print (phani.display_balance())
    '''
    Traceback (most recent call last):
        File "third.py", line 41, in <module>
        print (phani.display_balance())
        File "third.py", line 10, in display_balance
        print ("my balance is {}".format(balance))
        NameError: name 'balance' is not defined
    '''
## variables/data
## class data or variable - variable associated with your class
## instance data or variable - variable associated with your instance.
    print (dir(Account))
    '''
    ['__class__', '__delattr__', '__dict__', '__dir__', '__doc__', '__eq__', 
    '__format__', '__ge__', '__getattribute__', '__gt__', '__hash__', '__init__',
    '__init_subclass__', '__le__', '__lt__', '__module__', '__ne__', '__new__', 
    '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__',
    '__str__', '__subclasshook__', '__weakref__', 'balance', 'display_balance']
    '''
    print (Account.balance) # class variable  # 0
    print (phani.balance)   # instance variable # 0
    #phani.balance = 100
    #print (Account.balance) # class variable # 0
    #print (phani.balance)   # instance variable # 100
    #Account.balance = 200
    #print (Account.balance) # class variable # 200
    #print (phani.balance)   # instance variable # 200
    
    
    # call our function
    print ("balance of phani - {}".format(phani.balance))
    phani.balance = 2000
    print (phani.display_balance())
    
## Mounika
    mounika = Account()
    print ("balance of mounika - {}".format(mounika.balance))
    mounika.balance = 5000
    print (mounika.display_balance())
    
## bendu
    bendu = Account()
    print ("Balance of bendu - {}".format(bendu.balance))
    bendu.balance = 7000
    print (bendu.display_balance())
    
    


