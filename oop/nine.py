#!/usr/bin/python
# Encapsulation
# reference - https://radek.io/2011/07/21/private-protected-and-public-in-python/

## private
class Cup:
    def __init__(self):
        self.color = None  
        self.__content = None # private

    def fill(self, beverage): 
        self.__content = beverage # private

    def empty(self):
        self.__content = None  # private
        
    def display(self):
        return ("my cup has - {}".format(self.__content))

# Main
GreenCup = Cup()
'''
print (GreenCup.__content)
Traceback (most recent call last):
  File "nine.py", line 21, in <module>
    print (GreenCup.__content)
AttributeError: 'Cup' object has no attribute '__content'
'''
GreenCup.fill("lassi")
print (GreenCup.display())
print (GreenCup.empty())
print (GreenCup.display())

## protected
#class Cup:
#    def __init__(self):
#        self._color = None  # protected
#        self._content = None
#
#    def fill(self, beverage): # protected
#        self._content = beverage
#
#    def empty(self):
#        self._content = None
#        
#    def display(self):
#        print ("my cup has - {}".format(self._content))
#
### main
#redcup = Cup()
#redcup._content = "tea"
#print (redcup._content)


## public
#class Cup:
#    def __init__(self):
#        self.color = None  # public
#        self.content = None
#
#    def fill(self, beverage): # public
#        self.content = beverage
#
#    def empty(self):
#        self.content = None
#        
#    def display(self):
#        print ("my cup has - {}".format(self.content))
#        
### main
#Yellow_cup = Cup()
#print (Yellow_cup.color)
#print (Yellow_cup.content)
#Yellow_cup.color = "yellow"
#Yellow_cup.content = "coffee"
#print (Yellow_cup.color)
#print (Yellow_cup.content)