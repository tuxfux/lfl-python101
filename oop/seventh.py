#!/usr/bin/python
# custom exception creation.

## child class of Exception base class
class InvalidAge(Exception):
    def __init__(self,age):
        self.age = age

def validate_age(age):
    if age > 18:
        return ("Congrates!! you are ready to do bad things")
    else:
        raise InvalidAge(age)

#Main
if __name__ == '__main__':
    age = int(input("please enter your age:"))
    
    try:
        (validate_age(age))
    except Exception as e:  # e is a instance of Exception class
        print ("You are still as kiddo.. better luck next time -{}".format(e.age))
    else:
        print (validate_age(age))
    











#"""
#In [4]: import builtins as exceptions
#
#In [5]: print (dir(exceptions))
#['ArithmeticError', 'AssertionError', 
#'AttributeError', 'BaseException', 'BlockingIOError', 'BrokenPipeError', 
#'BufferError', 'BytesWarning', 'ChildProcessError', 'ConnectionAbortedError', 
#'ConnectionError', 'ConnectionRefusedError', 'ConnectionResetError', 
#'DeprecationWarning', 'EOFError', 'Ellipsis', 'EnvironmentError', 'Exception',
# 'False', 'FileExistsError', 'FileNotFoundError', 'FloatingPointError', 
# 'FutureWarning', 'GeneratorExit', 'IOError', 'ImportError', 'ImportWarning', 
# 'IndentationError', 'IndexError', 'InterruptedError', 'IsADirectoryError', 
# 'KeyError', 'KeyboardInterrupt', 'LookupError', 'MemoryError', 
# 'ModuleNotFoundError', 'NameError', 'None', 'NotADirectoryError', 
# 'NotImplemented', 'NotImplementedError', 'OSError', 'OverflowError', 
# 'PendingDeprecationWarning', 'PermissionError', 'ProcessLookupError', 
# 'RecursionError', 'ReferenceError', 'ResourceWarning', 'RuntimeError', 'RuntimeWarning', 'StopAsyncIteration', 
# 'SyntaxError', 'SyntaxWarning', 'SystemError', 'SystemExit', 'TabError', 'TimeoutError', 'True', 'TypeError', 
# --- truncated data --- 
# 
# 
#In [10]: raise SyntaxError ("Please clean your glasses")
#Traceback (most recent call last):
#
#  File "C:\Users\tuxfux\Anaconda3\lib\site-packages\IPython\core\interactiveshell.py", line 3325, in run_code
#    exec(code_obj, self.user_global_ns, self.user_ns)
#
#  File "<ipython-input-10-865bb9c388b5>", line 1, in <module>
#    raise SyntaxError ("Please clean your glasses")
#
#  File "<string>", line unknown
#SyntaxError: Please clean your glasses
#
#
#"""