#!/usr/bin/python

def create_account():
    return {'balance':0}

def my_deposit(account):
    account['balance'] = account['balance'] + 1000
    return account['balance']

def my_withdraw(account):
    account['balance'] =  account['balance'] - 200
    return account['balance']

## main

# phani/bendu both of them are different entities
# phani - bank account
# salary nov 1
phani = create_account()
print (phani)
print (my_deposit(phani))
# movie
print (my_withdraw(phani))

## bendu - bank account
# salary nov 1
bendu = create_account()
print (bendu)
print (my_deposit(bendu))
print (my_withdraw(bendu))