#!/usr/bin/python
# -*- coding: utf-8 -*-
# constructor - __init__ - special method
# constructor is the first function which get called when you create an instance.
# Account -> parent class
# MinBalanceAccount -> child class
## in inheritance we cannot import the __init__ variable/values implicitily

# salaried people - overdraft
# parent class
class Account(object):
    def __init__(self,amount=0):
        self.balance = amount
    def my_deposit(self,salary):
        self.balance = self.balance + salary
    def my_withdraw(self,expense):
        self.balance = self.balance - expense
    def display_balance(self):
        return ("My balance is - {}".format(self.balance))
    
## A student - no zero balance account - 1000
## child class
class MinBalanceAccount(Account):
    def __init__(self):
        Account.__init__(self)
    def my_withdraw(self,expense):
        if self.balance - expense < 1000:
            print ("Please call your dad !!! ")
        else:
            Account.my_withdraw(self,expense)
 
## Main
## student
print ("Vikram details")
vikram = MinBalanceAccount()
print (vikram.display_balance())
## vikram pings his dad.
vikram.my_deposit(5000)
print (vikram.display_balance())
vikram.my_withdraw(3000)
print (vikram.display_balance())
vikram.my_withdraw(1500)
print (vikram.display_balance())
vikram.my_withdraw(900)
print (vikram.display_balance())
        
# salary
print ("Bendus details")
bendu = Account()
print (bendu.display_balance())
bendu.my_deposit(10000) 
print (bendu.display_balance())
bendu.my_withdraw(5000) # emi
bendu.my_withdraw(3000) # school fee
bendu.my_withdraw(5000) # gold
print (bendu.display_balance())
