#!/usr/bin/python

balance = 0

def my_deposit():
    global balance
    balance = balance + 1000
    return balance

def my_withdraw():
    global balance
    balance =  balance - 200
    return balance

## main

# phani/bendu both of them are different entities
# phani - bank account
# salary nov 1
print (my_deposit())
# movie
print (my_withdraw())

## bendu - bank account
# salary nov 1
print (my_deposit())