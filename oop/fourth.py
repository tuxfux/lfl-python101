#!/usr/bin/python
# -*- coding: utf-8 -*-

class Account(object):
    balance = 0
    def my_deposit(self):
        self.balance = self.balance + 1000
    def my_withdraw(self):
        self.balance = self.balance - 200
    def display_balance(self):
        return ("My balance is - {}".format(self.balance))
    
## Main
if __name__ == '__main__':
    phani = Account()
    print ("balance of phani - {}".format(phani.balance))
    phani.my_deposit()
    phani.my_withdraw()
    print (phani.display_balance())
    
## Mounika
    mounika = Account()
    print ("balance of mounika - {}".format(mounika.balance))
    mounika.my_deposit()
    mounika.my_withdraw()
    print (mounika.display_balance())
    
## bendu
    bendu = Account()
    print ("Balance of bendu - {}".format(bendu.balance))
    bendu.my_deposit()
    bendu.my_deposit()
    bendu.my_withdraw()
    print (bendu.display_balance())

## see everyone balance    
    print ("balance of phani - {}".format(phani.balance))
    print ("balance of mounika - {}".format(mounika.balance))
    print ("Balance of bendu - {}".format(bendu.balance))
    
