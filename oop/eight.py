#!/usr/bin/python
# polymorphism - https://rszalski.github.io/magicmethods/
# Method overloading
# -*- coding: utf-8 -*-
# rationalnumbers
# 1/2 + 1/3 = 5/6
# (1*3 + 2*1)/2*3 = 5/6

class RationalNumber:
    def __init__(self,numerator,denominator=1):
        self.n = numerator
        self.d = denominator
        
    def __add__(self,other):
        
        if not isinstance(other,RationalNumber):
            other = RationalNumber(other)
        
        n = self.n * other.d + self.d * other.n
        d = self.d * other.d
        return RationalNumber(n,d)
    
    def __str__(self):
        return ("{}/{}".format(self.n,self.d))
    
    __repr__ = __str__
        
        
        
## Main
if __name__ == '__main__':
## RationalNumber class
  a = RationalNumber(1,2)  # a.n=1,a.d=2
  b = RationalNumber(1,3)  # b.n=1,b.d=2
  print (a + b)  # a.__add__(b) - __add__ of RationalNumber class
## str class  
  c = "Python" # instance of str class
  d = " Rocks" # instance of str class
  print (c + d) # __add__ of str class
## int class
  e = 1  # instance of int class
  f = 20 # instance of int class
  print (e + f) # __add__ of int class
## RationalNumber + Integer
## g is instance of the RationalNumber class
## h is instance of the instance class
  g = RationalNumber(1,2) # 1/2
  h = 2                   # 2/1
  print (g + h) # g.__add__(h) -> 1/2 + 2/1 = 5/2
  
#In [1]: a = 1
#
#In [2]: type(a)
#Out[2]: int
#
#In [3]: isinstance?
#Signature: isinstance(obj, class_or_tuple, /)
#Docstring:
#Return whether an object is an instance of a class or of a subclass thereof.
#
#A tuple, as in ``isinstance(x, (A, B, ...))``, may be given as the target to
#check against. This is equivalent to ``isinstance(x, A) or isinstance(x, B)
#or ...`` etc.
#Type:      builtin_function_or_method
#
#In [4]: isinstance(a,int)
#Out[4]: True
#
#In [5]: isinstance(a,str)
#Out[5]: False

