# Function
# why do we need functions ?
A function is block of code , which can be called from within a program.
The function can be called N number of times


```python
# how to define a function
# def -> defining a function.
# myfunc -> name of the function
# () -> passing your parameters across to the function.By default we need not always pass arguments/parameters.
# 
```


```python
def myfunc():
    print ("welcome to the functions class")
```


```python
# lets call the function
```


```python
myfunc() # call the already defined function.
```

    welcome to the functions class
    


```python
# A function can be called multiple times.
# Every function returns some value.
# if there is no return value , None is retuned as a default.
```


```python
print (myfunc())
```

    welcome to the functions class
    None
    


```python
## i dont to return None
# every function should have at least one return value.
# return is not a print statement. 
# Return marks the end of the function and the control goes back to the main program.
```


```python
def myfunc():
    return ("welcome to the functions class")  # i am done with this function.
    print ("line 1")
    print ("line 2")
    print ("line 3")
```


```python
print (myfunc())
```

    welcome to the functions class
    


```python
## namespaces
# A container of variables.
# local namespaces/variables or global namespaces/variables
```


```python
## Local namespaces or variables
# variables defined within a function are called as local variables.
# The life span of a local variable is during the run time of the function.
# locals() function which shows us local variables namespace.
```


```python
def my_local_fun():
    print (locals())  # locals() function shows your local namespace.
    my_salary = "20"   # local variables/local namespace
    print (locals())
    return (my_salary)
```


```python
print (my_local_fun())
```

    {}
    {'my_salary': '20'}
    20
    


```python
print (my_salary)
```


    ---------------------------------------------------------------------------

    NameError                                 Traceback (most recent call last)

    <ipython-input-19-33f3ca85f563> in <module>
    ----> 1 print (my_salary)
    

    NameError: name 'my_salary' is not defined



```python
## global namespace or global variables
# globals() show us the global namespaces
```


```python
from pprint import pprint
print (pprint(globals()))
```

    {'In': ['',
            '# Function',
            '# Function\n'
            '# why do we need functions ?\n'
            "'''\n"
            'A function is block of code , which can be called from within a '
            'program.\n'
            'The function can be called N number of times\n'
            "'''",
            'def myfunc():\n    print ("welcome to the functions class")',
            'myfunc() # call the already defined function.',
            'print myfunc()',
            'print (myfunc())',
            'print (myfunc())',
            'def myfunc():\n    return ("welcome to the functions class")',
            'print (myfunc())',
            'def myfunc():\n'
            '    return ("welcome to the functions class")\n'
            '    print ("line 1")\n'
            '    print ("line 2")\n'
            '    print ("line 3")',
            'def myfunc():\n'
            '    return ("welcome to the functions class")\n'
            '    print ("line 1")\n'
            '    print ("line 2")\n'
            '    print ("line 3")',
            'print (myfunc())',
            'def my_local_fun():\n    my_salary = "20"\n    return (my_salary)',
            'print (my_local_fun())',
            'print (my_salary)',
            'def my_local_fun():\n'
            '    print (locals())\n'
            '    my_salary = "20"   # local variables/local namespace\n'
            '    print (locals())\n'
            '    return (my_salary)',
            'print (my_local_fun())',
            'print (my_local_fun())',
            'print (my_salary)',
            'print (globals())',
            'from pprint import pprint\npprint(globals())',
            'from pprint import pprint\nprint (pprint(globals()))'],
     'Out': {2: '\n'
                'A function is block of code , which can be called from within a '
                'program.\n'
                'The function can be called N number of times\n'},
     '_': '\n'
          'A function is block of code , which can be called from within a '
          'program.\n'
          'The function can be called N number of times\n',
     '_2': '\n'
           'A function is block of code , which can be called from within a '
           'program.\n'
           'The function can be called N number of times\n',
     '__': '',
     '___': '',
     '__builtin__': <module 'builtins' (built-in)>,
     '__builtins__': <module 'builtins' (built-in)>,
     '__doc__': 'Automatically created module for IPython interactive environment',
     '__loader__': None,
     '__name__': '__main__',
     '__package__': None,
     '__spec__': None,
     '_dh': ['C:\\Users\\tuxfux\\Documents\\lfl-python101'],
     '_i': 'from pprint import pprint\npprint(globals())',
     '_i1': '# Function',
     '_i10': 'def myfunc():\n'
             '    return ("welcome to the functions class")\n'
             '    print ("line 1")\n'
             '    print ("line 2")\n'
             '    print ("line 3")',
     '_i11': 'def myfunc():\n'
             '    return ("welcome to the functions class")\n'
             '    print ("line 1")\n'
             '    print ("line 2")\n'
             '    print ("line 3")',
     '_i12': 'print (myfunc())',
     '_i13': 'def my_local_fun():\n    my_salary = "20"\n    return (my_salary)',
     '_i14': 'print (my_local_fun())',
     '_i15': 'print (my_salary)',
     '_i16': 'def my_local_fun():\n'
             '    print (locals())\n'
             '    my_salary = "20"   # local variables/local namespace\n'
             '    print (locals())\n'
             '    return (my_salary)',
     '_i17': 'print (my_local_fun())',
     '_i18': 'print (my_local_fun())',
     '_i19': 'print (my_salary)',
     '_i2': '# Function\n'
            '# why do we need functions ?\n'
            "'''\n"
            'A function is block of code , which can be called from within a '
            'program.\n'
            'The function can be called N number of times\n'
            "'''",
     '_i20': 'print (globals())',
     '_i21': 'from pprint import pprint\npprint(globals())',
     '_i22': 'from pprint import pprint\nprint (pprint(globals()))',
     '_i3': 'def myfunc():\n    print ("welcome to the functions class")',
     '_i4': 'myfunc() # call the already defined function.',
     '_i5': 'print myfunc()',
     '_i6': 'print (myfunc())',
     '_i7': 'print (myfunc())',
     '_i8': 'def myfunc():\n    return ("welcome to the functions class")',
     '_i9': 'print (myfunc())',
     '_ih': ['',
             '# Function',
             '# Function\n'
             '# why do we need functions ?\n'
             "'''\n"
             'A function is block of code , which can be called from within a '
             'program.\n'
             'The function can be called N number of times\n'
             "'''",
             'def myfunc():\n    print ("welcome to the functions class")',
             'myfunc() # call the already defined function.',
             'print myfunc()',
             'print (myfunc())',
             'print (myfunc())',
             'def myfunc():\n    return ("welcome to the functions class")',
             'print (myfunc())',
             'def myfunc():\n'
             '    return ("welcome to the functions class")\n'
             '    print ("line 1")\n'
             '    print ("line 2")\n'
             '    print ("line 3")',
             'def myfunc():\n'
             '    return ("welcome to the functions class")\n'
             '    print ("line 1")\n'
             '    print ("line 2")\n'
             '    print ("line 3")',
             'print (myfunc())',
             'def my_local_fun():\n    my_salary = "20"\n    return (my_salary)',
             'print (my_local_fun())',
             'print (my_salary)',
             'def my_local_fun():\n'
             '    print (locals())\n'
             '    my_salary = "20"   # local variables/local namespace\n'
             '    print (locals())\n'
             '    return (my_salary)',
             'print (my_local_fun())',
             'print (my_local_fun())',
             'print (my_salary)',
             'print (globals())',
             'from pprint import pprint\npprint(globals())',
             'from pprint import pprint\nprint (pprint(globals()))'],
     '_ii': 'print (globals())',
     '_iii': 'print (my_salary)',
     '_oh': {2: '\n'
                'A function is block of code , which can be called from within a '
                'program.\n'
                'The function can be called N number of times\n'},
     'exit': <IPython.core.autocall.ZMQExitAutocall object at 0x0000023306916F98>,
     'get_ipython': <bound method InteractiveShell.get_ipython of <ipykernel.zmqshell.ZMQInteractiveShell object at 0x00000233068B3780>>,
     'my_local_fun': <function my_local_fun at 0x0000023306AD57B8>,
     'myfunc': <function myfunc at 0x0000023306A14400>,
     'pprint': <function pprint at 0x0000023304B1BA60>,
     'quit': <IPython.core.autocall.ZMQExitAutocall object at 0x0000023306916F98>}
    None
    


```python
##
# A function always looks for variables in its local scope.
# if not found go back to the global scope.
# Local variable is given higher presedence than global varaiables.
```


```python
## case I
## defining a global variable
my_salary = 2000

def my_global_salary():
    print (locals())
    return (my_salary)
```


```python
print (my_global_salary())
```

    {}
    2000
    


```python
## case II
## defining a global variable
my_salary = 2000

def my_global_salary():
    my_salary = 500
    print (locals())
    return (my_salary)
```


```python
print (my_global_salary())
```

    {'my_salary': 500}
    500
    


```python
## case III
## defining a global variable
## no global variable defined.

def my_global_salary():
    print (locals())
    return (my_salary1)
```


```python
print (my_global_salary())
```

    {}
    


    ---------------------------------------------------------------------------

    NameError                                 Traceback (most recent call last)

    <ipython-input-31-c9af1ccd6b1a> in <module>
    ----> 1 print (my_global_salary())
    

    <ipython-input-30-1eadd7d8a920> in my_global_salary()
          5 def my_global_salary():
          6     print (locals())
    ----> 7     return (my_salary1)
    

    NameError: name 'my_salary1' is not defined



```python
## case IV
## defining a global variable
## no global variable defined.
# to do any local operation +,* etc, you should have a local scope/variable available to make any operations.

my_salary2 = 0

def my_global_salary():
    print (locals())
    my_salary2 = my_salary2 + 1000
    return (my_salary1)
```


```python
print (my_global_salary())
```

    {}
    


    ---------------------------------------------------------------------------

    UnboundLocalError                         Traceback (most recent call last)

    <ipython-input-45-c9af1ccd6b1a> in <module>
    ----> 1 print (my_global_salary())
    

    <ipython-input-44-d37308048d8b> in my_global_salary()
          7 def my_global_salary():
          8     print (locals())
    ----> 9     my_salary2 = my_salary2 + 1000
         10     return (my_salary1)
    

    UnboundLocalError: local variable 'my_salary2' referenced before assignment



```python
# global - keyword
# banking
```


```python
# case I
my_amount = 0
```


```python
# deposit
def my_deposit():
    my_amount = 0     # improvisation 1
    my_amount = my_amount + 1000
    print (locals())
    return my_amount
```


```python
# withdraw
def my_withdraw():
    my_amount = 0     # improvisation 1
    my_amount = my_amount - 500
    print (locals())
    return my_amount
```


```python
# phani - salary day
print (my_deposit()) # salary
```

    {'my_amount': 1000}
    1000
    


```python
# phani - EMI
print (my_withdraw()) # home loan
```

    {'my_amount': -500}
    -500
    


```python
## global keyword
## case 2
my_amount = 0  # global variable
```


```python
# deposit
def my_deposit():
    global my_amount
    print (locals())
    my_amount = my_amount + 1000
    return my_amount
```


```python
# withdraw
def my_withdraw():
    global my_amount
    print (locals())
    my_amount = my_amount - 500
    return my_amount
```


```python
# phani - salary day
print (my_deposit()) # salary
```

    {}
    1000
    


```python
print (my_amount)
```

    1000
    


```python
# phani - EMI
print (my_withdraw()) # home loan
```

    {}
    500
    


```python
print (my_amount)
```

    500
    


```python
### parameters
```


```python
# Assignment : the arguments a and b are they passed as value or references(memory address).
## its passed as an object.
# google.
```


```python
## positional parameters
```


```python
def my_add(a,b):
    print (locals())
    addition = a + b
    return (addition)
```


```python
print (my_add(21,22))
```

    {'a': 21, 'b': 22}
    43
    


```python
print (my_add("cloud"," rocks"))
```

    {'a': 'cloud', 'b': ' rocks'}
    cloud rocks
    


```python
print (my_add(" rocks","clouds"))
```

    {'a': ' rocks', 'b': 'clouds'}
     rocksclouds
    


```python
# keyword parameters
```


```python
print (my_add(b=" rocks",a="clouds"))
```

    {'a': 'clouds', 'b': ' rocks'}
    clouds rocks
    


```python
# default parameters
```


```python
def my_multi(my_num,my_value=10):   # my_value => default
    for value in range(1,my_value+1):
        print ("{} * {} = {}".format(my_num,value,my_num*value))
```


```python
my_multi(2) # my_value=10
```

    2 * 1 = 2
    2 * 2 = 4
    2 * 3 = 6
    2 * 4 = 8
    2 * 5 = 10
    2 * 6 = 12
    2 * 7 = 14
    2 * 8 = 16
    2 * 9 = 18
    2 * 10 = 20
    


```python
my_multi(2,5)  # 2 and 5 are taken as positinal arguments
```

    2 * 1 = 2
    2 * 2 = 4
    2 * 3 = 6
    2 * 4 = 8
    2 * 5 = 10
    


```python
my_multi(my_num=2,my_value=5) # keybased arguments
```

    2 * 1 = 2
    2 * 2 = 4
    2 * 3 = 6
    2 * 4 = 8
    2 * 5 = 10
    


```python
## def putty
# def putty(hostname,port=22):
#     pass
# putty(google.com)         # i am connecting to google.com on port 22
# putty(google.com,port=23) # i am connecting to the google.com on port 23
```


```python
## *,**,*args,**kwargs
```


```python
## * -> unpacks the list values
```


```python
def my_add(a,b):
    return a + b
```


```python
my_list = ["linux"," rocks"]
print (my_list,type(my_list))
```

    ['linux', ' rocks'] <class 'list'>
    


```python
# method 1: Not recommended
# a = my_list[0]
# b = my_list[1]
# print (my_add(a,b))  
# print (my_add(my_list[0],my_list[1]))
# a,b = my_list  ## unpacking
print (my_add(*my_list))
```

    linux rocks
    


```python
# ** -> unpacks the dict values
```


```python
def my_add(c,d):
    return c + d
```


```python
my_dict = {'c':"linux",'d':" rocks"}
print (my_dict,type(my_dict))
```

    {'c': 'linux', 'd': ' rocks'} <class 'dict'>
    


```python
# method 1: Not recommended
# c = my_dict['c']
# d = my_dict['d']
# print (my_add(c,d))  
# print (my_add(my_dict['c'],my_dict['d']))
print (my_add(**my_dict))
```

    linux rocks
    


```python
# *args
```


```python
help(max)
```

    Help on built-in function max in module builtins:
    
    max(...)
        max(iterable, *[, default=obj, key=func]) -> value
        max(arg1, arg2, *args, *[, key=func]) -> value
        
        With a single iterable argument, return its biggest item. The
        default keyword-only argument specifies an object to return if
        the provided iterable is empty.
        With two or more arguments, return the largest argument.
    
    


```python
print (max(-1,-5))
```

    -1
    


```python
print (max(21,54,76,99,100))
```

    100
    


```python
## gmax is a user defined function.
# *args is used in the defination of the function.
# args is a tuple of values which are prested to your function.
```


```python
def gmax(*args):
    return (args)
```


```python
print (gmax(-1,-5))
print (gmax(21,54,76,99,100))
```

    (-1, -5)
    (21, 54, 76, 99, 100)
    


```python
## actual logic
def gmax(*args):
    print (locals())
    big = -1
    for value in args:
        if value > big:
            big = value
    return big
```


```python
print (gmax(-1,-5))
print (gmax(21,54,76,99,100))
```

    {'args': (-1, -5)}
    -1
    {'args': (21, 54, 76, 99, 100)}
    100
    


```python
## **kwargs - keyword arguments
# kwargs -> provide us a dictionary of the values for the argument
```


```python
## call center guy/gal
```


```python
def callme(**kwargs):
    return (kwargs)
```


```python
#
print (callme(name="kumar",addr="kphb"))
print (callme(name="kumar",maiden="vijaya",addr="kphb"))
```

    {'name': 'kumar', 'addr': 'kphb'}
    {'name': 'kumar', 'maiden': 'vijaya', 'addr': 'kphb'}
    


```python
def callme(**kwargs):
    if 'name' in kwargs:
        print ("my name is {}".format(kwargs['name']))
    if 'addr' in kwargs:
        print ("my address is {}".format(kwargs['addr']))
    if 'maiden' in kwargs:
        print ("my mother name is {}".format(kwargs['maiden']))
```


```python
callme(name="kumar",addr="kphb")

```

    my name is kumar
    my address is kphb
    


```python
callme(name="kumar",maiden="vijaya",addr="kphb")
```

    my name is kumar
    my address is kphb
    my mother name is vijaya
    


```python
callme(maiden="vijaya",addr="kphb")
```

    my address is kphb
    my mother name is vijaya
    
## a combination
##
def my_func(*args,**kwargs):
    passAssignments : https://github.com/zhiwehu/Python-programming-exercises/blob/master/100%2B%20Python%20challenging%20programming%20exercises.txt

```python
## map,filter and lambda
```


```python
help(map)
```

    Help on class map in module builtins:
    
    class map(object)
     |  map(func, *iterables) --> map object
     |  
     |  Make an iterator that computes the function using arguments from
     |  each of the iterables.  Stops when the shortest iterable is exhausted.
     |  
     |  Methods defined here:
     |  
     |  __getattribute__(self, name, /)
     |      Return getattr(self, name).
     |  
     |  __iter__(self, /)
     |      Implement iter(self).
     |  
     |  __next__(self, /)
     |      Implement next(self).
     |  
     |  __reduce__(...)
     |      Return state information for pickling.
     |  
     |  ----------------------------------------------------------------------
     |  Static methods defined here:
     |  
     |  __new__(*args, **kwargs) from builtins.type
     |      Create and return a new object.  See help(type) for accurate signature.
    
    


```python
def square(a):
    return a * a
```


```python
print (square(2))
print (square(4))
```

    4
    16
    


```python
# we want to pass some values to a function.
# the values will be passed to the function one by one and the output will be presented.
# 3.x
map(square,range(1,11))
```




    <map at 0x29c30e43eb8>


# 2.x
In [1]: def square(a):
   ...:     return a * a
   ...:

In [2]: map(square,range(1,11))
Out[2]: [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]

In [3]: map?
Docstring:
map(function, sequence[, sequence, ...]) -> list

Return a list of the results of applying the function to the items of
the argument sequence(s).  If more than one sequence is given, the
function is called with an argument list consisting of the corresponding
item of each sequence, substituting None for missing values when not all
sequences have the same length.  If the function is None, return a list of
the items of the sequence (or a list of tuples if more than one sequence).
Type:      builtin_function_or_method

```python
for value in map(square,range(1,11)):
    print (value)
```

    1
    4
    9
    16
    25
    36
    49
    64
    81
    100
    


```python
# example
def full_path(a):
    return "/tmp/" + a
    
file_name = ["a.txt","b.txt","c.txt","d.txt","e.txt"]

for value in map(full_path,file_name):
    print (value)
```

    /tmp/a.txt
    /tmp/b.txt
    /tmp/c.txt
    /tmp/d.txt
    /tmp/e.txt
    


```python
## filter
```


```python
help(filter)
```

    Help on class filter in module builtins:
    
    class filter(object)
     |  filter(function or None, iterable) --> filter object
     |  
     |  Return an iterator yielding those items of iterable for which function(item)
     |  is true. If function is None, return the items that are true.
     |  
     |  Methods defined here:
     |  
     |  __getattribute__(self, name, /)
     |      Return getattr(self, name).
     |  
     |  __iter__(self, /)
     |      Implement iter(self).
     |  
     |  __next__(self, /)
     |      Implement next(self).
     |  
     |  __reduce__(...)
     |      Return state information for pickling.
     |  
     |  ----------------------------------------------------------------------
     |  Static methods defined here:
     |  
     |  __new__(*args, **kwargs) from builtins.type
     |      Create and return a new object.  See help(type) for accurate signature.
    
    


```python
def my_even(a):
    if a % 2 == 0:
        return 'even'  # True
```


```python
# This function is True since it returned a value.
print (my_even(10)) 
```

    even
    


```python
# This function if NOT True/False since it returned a None.
print (my_even(3))
```

    None
    


```python
## 
filter(my_even,range(1,11))
```




    <filter at 0x29c30e678d0>




```python
for value in filter(my_even,range(1,11)):
    print (value)
```

    2
    4
    6
    8
    10
    


```python
# example
def name_length(a):
    if len(a) >= 5:
        return True

my_names = ["phanindra","bendu","vikram","kumar","san"]

## 
print (name_length("bendu"))

for name in filter(name_length,my_names):
    print (name)
```

    True
    phanindra
    bendu
    vikram
    kumar
    
# 2.x
In [11]: filter?
Docstring:
filter(function or None, sequence) -> list, tuple, or string

Return those items of sequence for which function(item) is true.  If
function is None, return the items that are true.  If sequence is a tuple
or string, return the same type, else return a list.
Type:      builtin_function_or_method

In [12]: def my_even(a):
    ...:     if a % 2 == 0:
    ...:         return 'even'
    ...:

In [13]: filter(my_even,range(1,11))
Out[13]: [2, 4, 6, 8, 10]

```python
## lambda - A nameless function
```


```python
# map
# case I : we are using a user defined function square and later passing range of values.
# we are computing them using map.
def square(a):
    return a * a
```


```python
for value in map(square,range(1,11)):
    print (value)
```

    1
    4
    9
    16
    25
    36
    49
    64
    81
    100
    


```python
# case II:
# we are not using the user defined function square.
# we are using lambda to simulate the square function.
print (lambda a:a*a,range(1,11))
```

    <function <lambda> at 0x0000029C30E6B9D8> range(1, 11)
    


```python
for value in map(lambda a:a*a,range(1,11)):
    print (value)
```

    1
    4
    9
    16
    25
    36
    49
    64
    81
    100
    


```python
# filter
def my_even(a):
    if a % 2 == 0:
        return 'even'  # True
```


```python
for value in filter(my_even,range(1,11)):
    print (value)
```

    2
    4
    6
    8
    10
    


```python
# lambda
for value in filter(lambda a:a%2==0,range(1,11)):
    print (value)
```

    2
    4
    6
    8
    10
    


```python
## Assignment  - Read over these notes
```


```python
https://www.geeksforgeeks.org/lambda-filter-python-examples/
https://github.com/R4meau/46-simple-python-exercises/blob/master/exercises/ex31.py
https://towardsdatascience.com/understanding-the-use-of-lambda-expressions-map-and-filter-in-python-5e03e4b18d09
```
