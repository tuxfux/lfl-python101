#!/usr/bin/python
# -*- coding: utf-8 -*-
# http://simeonfranklin.com/blog/2012/jul/1/python-decorators-in-12-steps/
## tomorrow
## static method
## class method
## setters
## getters

## case II
# my function

# decorator
def topwrapper(func):
    def downwrapper(*args,**kwargs):
        try:
            func(*args,**kwargs)
        except Exception as e:
            return ("My exception is {}".format(e))
        else:
            return func(*args,**kwargs)
    return downwrapper


## my actual function
@topwrapper            #my_add = topwrapper(my_add)
def my_add(a,b):
    return a + b

@topwrapper            #my_div = topwrapper(my_div)
def my_div(a,b):
    return a/b

@topwrapper
def my_multi(a,b):
    return a * b
## my_add(2,3) (2,3) -> *args
## my_add(a=2,b=3) -> **kwargs
## old way of doing things   
#my_add = topwrapper(my_add)
#my_div = topwrapper(my_div)
#print (my_add)

print (my_multi(11,11))
print (my_multi('b','a'))
print (my_add(11,22))
print (my_add(a=11,b=22))
print (my_add(11,"linux"))
print (my_div(22,11))
print (my_div(a=22,b=11))
print (my_div(22,0))

## case I: not using decorators
#def my_add(a,b):
#    try:
#        a + b
#    except Exception as e:
#        return ("My exception is {}".format(e))
#    else:
#        return (a + b)
#
#def my_div(a,b):
#    try:
#        a/b
#    except Exception as e:
#        return ("My exception is {}".format(e))
#    else:
#        return a/b
#
### Main
#print (my_div(22,11))
#print (my_div(22,0))
#print (my_add(11,22))
#print (my_add(11,"linux"))

