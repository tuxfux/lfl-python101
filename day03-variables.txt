Python 2.7.16 |Anaconda, Inc.| (default, Mar 14 2019, 15:42:17) [MSC v.1500 64 bit (AMD64)]
Type "copyright", "credits" or "license" for more information.

IPython 5.8.0 -- An enhanced Interactive Python.
?         -> Introduction and overview of IPython's features.
%quickref -> Quick reference.
help      -> Python's own help system.
object?   -> Details about 'object', use 'object??' for extra details.

# variables

# In python we dont need type casting.

# strings

my_string = "python"


print my_string
python

# indexing and slicking

# A string is a sequence of characters

# p  y  t  h  o  n 

# 0  1  2  3  4  5 => left to right => +ve indexing

# p  y  t  h  o  n

# 0  1  2  3  4  5 => left to right => +ve indexing

# -6 -5 -4 -3 -2 -1 => right to left => -ve indexing

my_string[0]

Out[13]: 'p'

my_string[4]
Out[14]: 'o'


# i want the last charcater of a string



my_string[-1]
Out[16]: 'n'

# slicing

# p  y  t  h  o  n

# 0  1  2  3  4  5 => left to right => +ve indexing

# -6 -5 -4 -3 -2 -1 => right to left => -ve indexing

my_string[0:3] # 0 till 3
Out[22]: 'pyt'

my_string[3:9]
Out[23]: 'hon'

my_string[1:4]
Out[24]: 'yth'

my_string[:3] # 0 till 3
Out[25]: 'pyt'

my_string[3:]
Out[26]: 'hon'

## concatination

my_string[:3] + my_string[3:]
Out[28]: 'python'

'dusshera' + ' october'
Out[29]: 'dusshera october'

kumar
Traceback (most recent call last):

  File "<ipython-input-30-ff4dc63a828c>", line 1, in <module>
    kumar

NameError: name 'kumar' is not defined

# task

my_string="python"

# output => tython

# strings are immutable

my_string[0]
Out[36]: 'p'

my_string[0]="t"
Traceback (most recent call last):

  File "<ipython-input-37-68494f93657b>", line 1, in <module>
    my_string[0]="t"

TypeError: 'str' object does not support item assignment




't' + my_string[1:]
Out[38]: 'tython'

't' + my_string[1:9]
Out[39]: 'tython'

my_string[:-6]+my_string[2:] 
Out[40]: 'thon'

my_string[2] + my_string[1:]
Out[41]: 'tython'


## walk over the str object

my_string.replace?
Docstring:
S.replace(old, new[, count]) -> string

Return a copy of string S with all occurrences of substring
old replaced by new.  If the optional argument count is
given, only the first count occurrences are replaced.
Type:      builtin_function_or_method

my_string.replace("p","t")
Out[44]: 'tython'

my_string
Out[45]: 'python'

## walk over each function of str object and play with each function

my_string.capitalize?
Docstring:
S.capitalize() -> string

Return a copy of the string S with only its first character
capitalized.
Type:      builtin_function_or_method

my_string.capitalize()
Out[48]: 'Python'

# slicing

my_string
Out[51]: 'python'

my_string[0:6]
Out[52]: 'python'

my_string[:]
Out[53]: 'python'

my_string[::1]
Out[54]: 'python'

my_string[::2]
Out[55]: 'pto'

my_string[1::2]
Out[56]: 'yhn'

my_string[::-1]
Out[57]: 'nohtyp'

# p  y  t  h  o  n

# 0  1  2  3  4  5 => left to right => +ve indexing

# -6 -5 -4 -3 -2 -1 => right to left => -ve indexing


my_string[-4:]
Out[61]: 'thon'

my_string[-4:-1]
Out[62]: 'tho'

my_string[-4:0]
Out[63]: ''

my_string[-4:6]
Out[64]: 'thon'

# playing with numbers

1 + 1
Out[67]: 2

25 + 25 / 2
Out[68]: 37

(25 + 25) / 2
Out[69]: 25

25 % 2
Out[70]: 1

25 / 2
Out[71]: 12

type(25)
Out[72]: int

25 / 2.0
Out[73]: 12.5

type(12.5)
Out[74]: float

float(12)
Out[75]: 12.0

float(12)/2
Out[76]: 6.0

float(25)/2
Out[77]: 12.5

## BODMAS for arthemetic calcuations

# printing variables

my_name = "santosh"

my_school = "De paul"

my_town = "township"

my_commute = "bus"

print "my name is my_name"
my name is my_name

print "my name is " ,my_name

my name is  santosh

# type 1

print "my name is " ,my_name , "My school name is ",  my_school , 'My town is a small', my_town, 'i commute on a ', my_commute
my name is  santosh My school name is  De paul My town is a small township i commute on a  bus

# type 2

# %s -> string,%f -> float , %d -> digit


print "my name is %s. My school name is %s.My town is a small %s.I commute on %s" %(my_name,my_school,my_town,my_commute)
my name is santosh. My school name is De paul.My town is a small township.I commute on bus

# format

print "my school name is %s.I love my %s" %(my_school,my_school)
my school name is De paul.I love my De paul

print "my school name is {}.I love my {}".format(my_school,'st.xaviers')
my school name is De paul.I love my st.xaviers

print "my school name is {0}.I love my {0}".format(my_school,'st.xaviers')
my school name is De paul.I love my De paul

print "my school name is {1}.I love my {1}".format(my_school,'st.xaviers')
my school name is st.xaviers.I love my st.xaviers

print "my school name is {ms}.I love my {ms}".format(ms=my_school,sx='st.xaviers')
my school name is De paul.I love my De paul

## Assignment
http://www.pythonchallenge.com/pc/def/0.html

2 ** 38
Out[99]: 274877906944L

2.0 ** 38
Out[100]: 274877906944.0

import math

math.pow?
Docstring:
pow(x, y)

Return x**y (x to the power of y).
Type:      builtin_function_or_method

math.pow(2,38)

Out[103]: 274877906944.0

####
## taking input
####

name = input("what is your name:")

what is your name:santosh

print (name)
santosh

print name
  File "<ipython-input-3-101dadbc83c5>", line 1
    print name
             ^
SyntaxError: Missing parentheses in call to 'print'. Did you mean print(name)?




print (type(name))
<class 'str'>

num = input("please enter the number:")

please enter the number:10

print (type(num))

<class 'str'>

num + num
Out[7]: '1010'

# int,float

num = int(input("please enter your number:"))

please enter your number:10

print num,type(num)
  File "<ipython-input-10-4a48be606ea3>", line 1
    print num,type(num)
            ^
SyntaxError: invalid syntax




print (num,type(num))
10 <class 'int'>

num = int(input("please enter your number:"))

please enter your number:a
Traceback (most recent call last):

  File "<ipython-input-12-de0714f293af>", line 1, in <module>
    num = int(input("please enter your number:"))

ValueError: invalid literal for int() with base 10: 'a'


## 2.x

raw_input,input
