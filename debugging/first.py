#!/usr/bin/python
# -*- coding: utf-8 -*-

import pdb
version = 1.0

def my_add(a,b):
    ''' This is for addition of numbers - int and str  '''
    a = int(a)
    b = int(b)
    result = a + b
    return result

def my_sub(a,b):
    ''' This is for substraction of numbers - sub '''
    if a > b:
        return a - b
    else:
        return b  - a

def my_multi(a,b):
    ''' This is for multiplication of numbers - multi '''
    return a * b

def my_div(a,b):
    ''' This is for division of the numbers - div '''
    return a/b

# Main
if __name__ == '__main__':
    print ("Welcome to the calculator")
    print ("We are going to learn about debugging")
    pdb.set_trace()
    print ("Addition of two numbers :{}".format(my_add(1,2)))
    print ("substraction of two numbers :{}".format(my_sub(21,41)))
    print ("Multiplication of two numbers :{}".format(my_multi(2,20)))
    print ("Division of two numbers :{}".format(my_div(20,2)))
