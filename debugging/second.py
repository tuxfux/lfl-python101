#!/usr/bin/python
# -*- coding: utf-8 -*-
## 1L iteration 
## value -> abc -> error
## when i encounter values as abc

## example:
'''
it should do a pit stop when we hit on value == 6

'''

import pdb

pdb.set_trace()
for value in range(1,11):
    print ("value is {}".format(value))

