#!/usr/bin/python
# -*- coding: utf-8 -*-
## todo - cannot use a string pattern on a bytes-like object
import re
# read the file
read_email = open('email.txt',mode="r")
store_email = read_email.read()
read_email.close()

# compile
reg = re.compile('''
                 [0-9a-z.-]+  # my email name having numbers and strings
                 @            # seperator - @
                 [a-z.]+      # domain address
                 ''',re.I|re.M|re.X)

## find all combinations
print (reg.findall(store_email))

