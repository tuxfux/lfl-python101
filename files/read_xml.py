#!/usr/bin/python
# -*- coding: utf-8 -*-

from xml.dom.minidom import parse


DOMTree = parse("my_movies.xml")
collection = DOMTree.documentElement
if collection.hasAttribute("shelf"):
   print ("Root element : {}".format(collection.getAttribute("shelf")))
   
# Get all the movies in the collection
movies = collection.getElementsByTagName("movie")

for movie in movies:
   print ("*****Movie*****")
   if movie.hasAttribute("title"):
      print ("Title: {}".format(movie.getAttribute("title")))

   type = movie.getElementsByTagName('type')[0]
   print ("Type: {}".format(type.childNodes[0].data))
   format = movie.getElementsByTagName('format')[0]
   print ("Format: {}".format(format.childNodes[0].data))
   rating = movie.getElementsByTagName('rating')[0]
   print ("Rating: {}".format(rating.childNodes[0].data))
   description = movie.getElementsByTagName('description')[0]
   print ("Description: {}".format(description.childNodes[0].data))

