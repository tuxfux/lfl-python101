#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  2 18:41:42 2019

@author: tuxfux
break: break will break me out of the loop.
exit: exit will kick me out of the program.

import sys

sys.exit?
Docstring:
exit([status])

Exit the interpreter by raising SystemExit(status).
If the status is omitted or None, it defaults to zero (i.e., success).
If the status is an integer, it will be used as the system exit status.
If it is another kind of object, it will be printed and the system
exit status will be one (i.e., failure).
Type:      builtin_function_or_method


outer loop
 inner loop
   break
   
reference: http://people.cs.pitt.edu/~alanjawi/cs449/code/shell/UnixSignals.htm
refrence: http://man7.org/linux/man-pages/man3/exit.3.html

Assignement:
- you can restrict the user to guess max 3 times.
- if num of attempts more than 3, better luck next time.
"""
import sys

yes_no = input("Do you want to play the game:(y/n)")
if yes_no == 'n' or yes_no =='no':
    sys.exit(0)

my_num= 7 # palm
#test = True

#while test:  # while loop allows if condition is true.
while True:
    guess_num = int(input("please guess the num:"))
    if guess_num > my_num:
        print ("buddy!! you guessed the number slightly larger")
    elif guess_num < my_num:
        print ("buddy!! you guessed the number slightly smaller")
    elif guess_num == my_num:
        print ("buddy!! congratulation you guessed right")
        #test = False
        break
        #sys.exit(0)
    
print ("please come back again !!!")